package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;

/**
 * Created by ranjan on 22/4/14.
 */

public class NameValueSplit implements UnaryFunction<String,String> {

    public String delimiter = ":";

    public NameValueSplit() {
    }

    public NameValueSplit(String delimiter) {
        this.delimiter = delimiter;
    }

    public String exec(String arg) {
        String[] words = arg.split(delimiter, 2);
        if (words.length > 1)
            return words[1].trim();
        else
            return null;
    }
}

package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by ranjan on 22/4/14.
 */
public class URLDecode implements UnaryFunction<String,String> {

    final static Log log = LogFactory.getLog(URLDecode.class);

    public URLDecode() {
    }

    public String exec(String arg) {
        try {
            return URLDecoder.decode(arg, "UTF-8");
        } catch( UnsupportedEncodingException ex ) {
            log.error("Error in exec : "+ex.getMessage(),ex);
            return null;
        }
    }
}

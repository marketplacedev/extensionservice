/**
 * Filename : TokenizerSplit
 * Description :
 * Date : 23 Jun, 2014
 * Owner : Clockwork Interviews Pvt., Ltd.,
 * Project : piqube-extensions
 * Contact : qube@piqube.com
 * History : 
 */
package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author <a href="mailto:me@arulraj.net">Arul</a>
 */
public class TokenizerSplit implements UnaryFunction<List, String> {

  String delimeter = ",";

  public TokenizerSplit() {
  }

  public TokenizerSplit(String delimeter) {
    this.delimeter = delimeter;
  }

  @Override
  public List<String> exec(String input) {
    List<String> list = new ArrayList<>();
    StringTokenizer tokenizer = new StringTokenizer(input, delimeter);
    while(tokenizer.hasMoreTokens()) {
      list.add(tokenizer.nextToken().trim());
    }
    return list;
  }
}

package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.net.URI;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by ranjan on 22/4/14.
 */
public class GetRedirectURL implements UnaryFunction<String,String> {

    final static Log log = LogFactory.getLog(GetRedirectURL.class);

    public String paramName;
    public GetRedirectURL(String paramName) {
        this.paramName = paramName;
    }

    @Override
    public String exec(String arg) {
        try {
            String redirUrl = null;
            URI uri = new URI(arg);
            List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");
            for( NameValuePair param : params ) {
                if ( param.getName().equals(paramName) ) {
                    redirUrl = param.getValue();
                    break;
                }
            }
            if ( redirUrl == null )
                return arg;
            redirUrl = URLDecoder.decode(redirUrl, "UTF-8");
            return redirUrl;

        } catch( Exception ex ) {
            log.error("Error in exec : "+ex.getMessage(),ex);
            return null;
        }
    }
}


/**
 * Filename : ReplaceString
 * Description :
 * Date : 26 Jun, 2014
 * Owner : Clockwork Interviews Pvt., Ltd.,
 * Project : piqube-extensions
 * Contact : qube@piqube.com
 * History : 
 */
package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;

/**
 * @author <a href="mailto:me@arulraj.net">Arul</a>
 */
public class ReplaceString implements UnaryFunction<String, String> {

  public enum ReplaceType {
    FIRST,
    LAST,
    ALL;
  }

  public String findStr;

  public String replaceStr;

  public ReplaceType replaceType;

  public ReplaceString(String findStr, String replaceStr) {
    this(findStr, replaceStr, ReplaceType.ALL);
  }

  public ReplaceString(String findStr, String replaceStr, ReplaceType replaceType) {
    this.findStr = findStr;
    this.replaceStr = replaceStr;
    this.replaceType = replaceType;
  }

  @Override
  public String exec(String input) {
    switch (replaceType) {
      case FIRST:
        input = input.replaceFirst(findStr, replaceStr);
        break;
      case LAST:
        input =  input.replaceFirst("(?s)(.*)" + findStr, "$1" + replaceStr);
        break;
      default:
        input = input.replace(findStr, replaceStr);
    }
    return input;
  }
}

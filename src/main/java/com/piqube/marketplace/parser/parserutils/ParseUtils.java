package com.piqube.marketplace.parser.parserutils;

import com.piqube.marketplace.parser.utils.UnaryFunction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.jsoup.nodes.Element;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Deprecated
/**
 * Use Utils with in the parsehelper. com.piqube.crawler.parserutils.parsehelpers
 */
public class ParseUtils {

    final static Log log = LogFactory.getLog(ParseUtils.class);

    @Deprecated
    public static class NameValueSplit implements UnaryFunction<String,String> {

        public String delimiter = ":";

        public NameValueSplit() {
        }

        public NameValueSplit(String name, String delimiter) {
            this.delimiter = delimiter;
        }

        public String exec(String arg) {
            String[] words = arg.split(delimiter,2);
            return words[1].trim();
        }
    }

    @Deprecated
    public static class URLDecode implements UnaryFunction<String,String> {

        public URLDecode() {
        }

        public String exec(String arg) {
            try {
                return URLDecoder.decode(arg,"UTF-8");
            } catch( UnsupportedEncodingException ex ) {
                log.error("Error in exec : "+ex.getMessage(),ex);
                return null;
            }
        }
    }

    @Deprecated
    public static class GetRedirectURL implements UnaryFunction<String,String> {

        public String paramName;
        public GetRedirectURL(String paramName) {
            this.paramName = paramName;
        }

        public String exec(String arg) {
            try {
                String redirUrl = null;
                URI uri = new URI(arg);
                List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");
                for( NameValuePair param : params ) {
                    if ( param.getName().equals(paramName) ) {
                        redirUrl = param.getValue();
                        break;
                    }
                }
                if ( redirUrl == null )
                    return null;
                redirUrl = URLDecoder.decode(redirUrl,"UTF-8");
                return redirUrl;

            } catch( Exception ex ) {
                log.error("Error in exec : "+ex.getMessage(),ex);
                return null;
            }
        }
    }

    @Deprecated
    public static class RegexFetcher implements UnaryFunction<String,String> {

        public String patternVar;
        public String patternStr;
        public Pattern pattern;

        public RegexFetcher(String patternVar, String patternStr) {
            this.patternVar = patternVar;
            this.patternStr = patternStr;
            this.pattern = Pattern.compile(this.patternStr);
        }

        @Override
        public String exec(String input) {
            Matcher matcher = pattern.matcher(input);
            if ( matcher.find() ) {
                return matcher.group(this.patternVar);
            }
            return null;
        }
    }

    @Deprecated
    public static class EmailFetcher implements UnaryFunction<Set<String>, Object> {

        @Override
        public Set<String> exec(Object input) {
            Pattern pattern = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b",
                    Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(input.toString());
            Set<String> emails = new HashSet<String>();
            while(matcher.find()) {
                emails.add(matcher.group());
            }
            return emails.isEmpty() ? null : emails;
        }

    }

    @Deprecated
    public static class LinkedInMemberIdFetcher implements UnaryFunction<String, Element> {

      @Override
      public String exec(Element input) {
        /**
         * TODO: have to make single Regex like newTrkInfo\s*=\s*'(\d*)\,'
         */
        if(input.data().contains("newTrkInfo = ")) {
          Pattern pattern = Pattern.compile("newTrkInfo =(.*)\\+");
          Matcher matcher = pattern.matcher(input.data());
          String retVal = null;
          if(matcher.find()) {
            retVal = matcher.group().trim();
            pattern = Pattern.compile("[0-9]\\d*");
            Matcher mm = pattern.matcher(retVal);
            if(mm.find()) {
              retVal = mm.group();
            }
          }
          return retVal;
        }
        return null;
      }

    }

}

package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ranjan on 22/4/14.
 */
public class EmailFetcher implements UnaryFunction<Set<String>, Object> {

    @Override
    public Set<String> exec(Object input) {
        Pattern pattern = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input.toString());
        Set<String> emails = new HashSet<String>();
        while(matcher.find()) {
            emails.add(matcher.group());
        }
        return emails.isEmpty() ? null : emails;
    }

}

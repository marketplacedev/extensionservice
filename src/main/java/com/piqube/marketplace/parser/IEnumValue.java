package com.piqube.marketplace.parser;

public interface IEnumValue<V> {
    public V get();
}

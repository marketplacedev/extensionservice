package com.piqube.marketplace.parser.sites.naukri;

import com.google.common.collect.Lists;
import com.piqube.marketplace.parser.CrawledSource;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils.ParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;
import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.marketplace.parser.utils.UnaryFunction;
import com.piqube.model.*;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;
import java.util.*;

/**
 * @author jenefar
 */
public class NaukriProfileParser implements Parser {
    public static Logger log = LoggerFactory.getLogger(NaukriProfileParser.class);

    static FileUtil util =new FileUtil();
    public static final String monthsRegex = "(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)";
    public static final String dateRegex =
            String.format( "(?i)(?<startMonth>%s)[\\s]+(?<startYear>[\\d]{4})[\\s]+to[\\s]+((?<endMonth>%s)[\\s]+(?<endYear>[\\d]{4})|(?<current>till date))", monthsRegex, monthsRegex );

    public  enum Sections implements IEnumValue<_SelectorMetaInfo> {

        NAME_SELECTOR(  new _SelectorMetaInfo(Arg.E(".nameCont .bkt4") ) ),
        DESIGNATION_SELECTOR( new _SelectorMetaInfo(Arg.E(".innerDetailsCont .desc .bkt4"))),
        LOCATION_SELECTOR(  new _SelectorMetaInfo( Arg.E(".tupBGN li:contains(Current Location)"),
                Arg.F(new ParseUtils.NameValueSplit()) ) ),
        PHOTO_SELECTOR( new _SelectorMetaInfo( Arg.E(".jseekImg[src]"), Arg.a("src") ) ),
        EMAIL_SELECTOR( new _SelectorMetaInfo( Arg.E(".emailCont .txtGreen")) ) ,

        SUMMARY_SELECTOR( new _SelectorMetaInfo( Arg.E(".content .bkt4") )),
        NOTICE_PERIOD_SELECTOR( new _SelectorMetaInfo(Arg.E(".innerDetailsCont label:contains(Notice Period) >div"))),

        SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E("#profile-skills ul li .endorse-item-name a "),Arg.list())),
        CURRENT_COMPANY( new _SelectorMetaInfo( Arg.E(".innerDetailsCont .desc .cOrg") ) ),
        CURRENT_SALARY_SELECTOR( new _SelectorMetaInfo( Arg.E(".exp-sal-loc-box > span ~ span") ) ),

        PROFILE_URL(new _SelectorMetaInfo( Arg.E("link[rel=canonical]"),Arg.a("href"))),
        KEY_SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E(".itSkill"),Arg.ownText()) ),
        IT_SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E(".section:contains(IT Skills) .tableDMNR tr td:eq(0)"), Arg.list() ) ),


        EXPERIENCE_SELECTOR( new _SelectorMetaInfo(
                Arg.E(".exp-container"), Arg.list(),
                Arg.Children(ExperienceSection.class) ) ),

        EDUCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".education-inner"),Arg.list(),
                Arg.Children(EducationSection.class) ) ),


        PROJECT_SELECTOR( new _SelectorMetaInfo( Arg.E(".project-details-box"),Arg.list(),
        Arg.Children(ProjectSection.class) ) );
        ;
        private _SelectorMetaInfo metaInfo;

        Sections(_SelectorMetaInfo info) {
            this.metaInfo = info;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }

    }


    public enum ExperienceSection implements IEnumValue<_SelectorMetaInfo> {

        COMPANY( new _SelectorMetaInfo( Arg.E(".org") ) ),

        DURATION( new _SelectorMetaInfo(Arg.E(".time"),Arg.ownText()) ),

        DESIGNATION(new _SelectorMetaInfo( Arg.E(".designation") ) ),

        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".details") ) ),


        ;
        public _SelectorMetaInfo metaInfo;
        private ExperienceSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {


        DEGREE(new _SelectorMetaInfo( Arg.E(".deg") ) ),
        MAJOR(new _SelectorMetaInfo( Arg.E("strong ~strong") ) ),

        UNIVERSITY(new _SelectorMetaInfo( Arg.E(".org")  ) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".detail"),Arg.ownText()  ) ),


        ;
        public _SelectorMetaInfo metaInfo;
        private EducationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public  enum ProjectSection implements IEnumValue<_SelectorMetaInfo> {

        NAME(new _SelectorMetaInfo( Arg.E(".title")) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".time-box")) ),
        CLIENT(new _SelectorMetaInfo( Arg.E(".details span:contains(Client) ~span"), Arg.text() ) ),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".details span:contains(Role Description) ~span"), Arg.text() ) ),
        ROLE(new _SelectorMetaInfo( Arg.E(".details span:contains(Role) ~span"), Arg.text() ) ),
        EMPLOYMENT_NATURE(new _SelectorMetaInfo( Arg.E(".details span:contains(Employment Nature) ~span"), Arg.text() ) ),


        ;
        public _SelectorMetaInfo metaInfo;
        private ProjectSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }



    public static class MemberIdNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            if (input.contains("-")) {
                String[] str = input.split("-");
                if (str.length > 0)
                    return str[1].trim();
                return null;
            } else {
                return null;
            }
        }
    }
    @Override
    public Person parsePerson(String htmlSource) throws ParsingException {

        Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
        Set<Map.Entry<Sections, Object>> entries = map.entrySet();
        for (Map.Entry<Sections, Object> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        Set<Map<EducationSection, Object>> educationFieldMap = (Set<Map<EducationSection, Object>>) map.get(Sections.EDUCATION_SELECTOR);
        Set<Map<ExperienceSection, Object>> experienceFieldMap = (Set<Map<ExperienceSection, Object>>) map.get(Sections.EXPERIENCE_SELECTOR);
        Set<Map<ProjectSection, Object>> projectFieldMap = (Set<Map<ProjectSection, Object>>) map.get(Sections.PROJECT_SELECTOR);

        String skills = (String) map.get(Sections.KEY_SKILLS_SELECTOR);
        List<String> skillSet = null;
        if(null!=skills) {
            skillSet = Arrays.asList(skills.split("\\s*,\\s*"));
        }
        Set<String> ITSkillSet=(Set)map.get(Sections.IT_SKILLS_SELECTOR);
        String name = (String) map.get(Sections.NAME_SELECTOR);
        String location = (String) map.get(Sections.LOCATION_SELECTOR);
        String profileSummary = (String) map.get(Sections.SUMMARY_SELECTOR);
        String currentCompany = (String) map.get(Sections.CURRENT_COMPANY);
        String photoUrl = (String) map.get(Sections.PHOTO_SELECTOR);
        String profileUrl = (String) map.get(Sections.PROFILE_URL);
        String currentDesignation = (String) map.get(Sections.DESIGNATION_SELECTOR);
        String email = (String) map.get(Sections.EMAIL_SELECTOR);
        String currentSalary = (String) map.get(Sections.CURRENT_SALARY_SELECTOR);




        Person person = new Person();

        if (!util.isEmpty(name)) {
            Name personName = getPersonName(name);
            person.setName(personName);
        }
        if (!util.isEmpty(profileSummary))
            person.setProfileSummary(util.replaceUnicodeWithASCII(profileSummary));

        if(!util.isEmpty(profileUrl))
            person.setProfileUrl(profileUrl);

        if(!util.isEmpty(currentSalary))
            person.setCurrentSalary(currentSalary);
        if(!util.isEmpty(photoUrl))
            person.setPhoto_url(photoUrl);
        if(!util.isEmpty(currentDesignation))
            person.setDesignation(currentDesignation);
        if(!util.isEmpty(currentCompany))
            person.setCurrentCompany(currentCompany);
        if(!util.isEmpty(location))
            person.setLocality(location);

        if(!util.isEmpty(email))
            person.setPrimaryEmail(email);
        if(!util.isEmpty(skillSet)) {
            Set<String> skill = new HashSet<>(skillSet);
            person.setSkills(Lists.newArrayList(skill));
            /*if (ITSkillSet.size() > 0) {
                ITSkillSet.addAll(skillSet);
                Set<String> skill = new HashSet<>(ITSkillSet);
                person.setSkills(Lists.newArrayList(skill));
            } else {
                Set<String> skill = new HashSet<>(skillSet);
                person.setSkills(Lists.newArrayList(skill));
            }*/
        }

        if (!util.isEmpty(educationFieldMap)) {
            List<Education> educationList = getPersonEducation(educationFieldMap);
            person.setEducation(educationList);
        }
        if (!util.isEmpty(experienceFieldMap)) {
            List<Experience> experienceList = getPersonExperience(experienceFieldMap);
            person.setExperience(experienceList);
        }
        if (!util.isEmpty(projectFieldMap)) {
            List<Projects> projectlist = getPersonProject(projectFieldMap);
            person.setProjects(projectlist);
        }
        person.setSource(CrawledSource.NAUKRI.name());
        person.setCrawledDate(new Date());

        System.out.println("Person:"+person.toJSON());
        return person;
    }

    private List<Projects> getPersonProject(Set<Map<ProjectSection, Object>> projectFieldMap) {
        List<Projects> projectList=new ArrayList<>();
        if (null != projectFieldMap) {
            int experienceCount=0;
            for (Map<ProjectSection, Object> item : projectFieldMap) {
                Projects project=new Projects();

                String name = (String) item.get(ProjectSection.NAME);
                String client = (String) item.get(ProjectSection.CLIENT);
                String designation = (String) item.get(ProjectSection.ROLE);
                String duration=(String) item.get(ProjectSection.DURATION);
                String description=(String) item.get(ProjectSection.DESCRIPTION);
                String employmentNature=(String) item.get(ProjectSection.EMPLOYMENT_NATURE);


                if (!util.isEmpty(name))
                    project.setName(name);
                if (!util.isEmpty(client))
                    project.setClient(client);
                if (!util.isEmpty(employmentNature))
                    project.setEmploymentNature(employmentNature);
                if (!util.isEmpty(designation))
                    project.setRole(designation);
                if (!util.isEmpty(description))
                    project.setDesc(description);
                if (!util.isEmpty(duration)) {

                    if(duration.contains("Team Size  "))
                    {
                        Map m1=splitString(duration,"Team Size  ");
                        duration=m1.get("left").toString();
                        try {
                            if (null != m1.get("right")) {
                                String teamSize=util.replaceUnicodeWithASCII(m1.get("right").toString());
                                project.setTeamSize(Integer.valueOf(teamSize));
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    if (duration.contains(" to ")) {
                        Map map = splitString(duration, " to ");
                        project.setStart((String) map.get("left"));
                        if (map.get("right").toString().contains("Till Date")) {
                            project.setEnd("Present");
                            project.setCurrent(true);
                        } else {
                            project.setEnd(map.get("right").toString());
                        }
                    }
                    else{
                        project.setEnd(duration);
                    }
                }

                projectList.add(project);
            }
        }
        return projectList;

    }


    private static List<Experience> getPersonExperience(Set<Map<ExperienceSection, Object>> experienceFieldMap) {
        List<Experience> experienceList=new ArrayList<>();
        if (null != experienceFieldMap) {
            int experienceCount=0;
            for (Map<ExperienceSection, Object> item : experienceFieldMap) {
                Experience experience=new Experience();
                experience.setResumeOrdinal(experienceCount);
                experienceCount=experienceCount+1;
                String company = (String) item.get(ExperienceSection.COMPANY);
                String designation = (String) item.get(ExperienceSection.DESIGNATION);
                String duration=(String) item.get(ExperienceSection.DURATION);
                String description=(String) item.get(ExperienceSection.DESCRIPTION);


                if (!util.isEmpty(company))
                    experience.setOrg(company);
                if (!util.isEmpty(designation))
                    experience.setTitle(designation);
                if (!util.isEmpty(description))
                    experience.setDesc(description);
                if (!util.isEmpty(duration)) {
                    if (duration.contains(" to ")) {
                        Map map = splitString(duration, " to ");
                        experience.setStart((String) map.get("left"));
                        if (map.get("right").toString().contains("Till Date")) {
                            experience.setEnd("Present");
                            experience.setCurrent(true);
                        } else {
                            experience.setEnd(map.get("right").toString());
                        }
                    }
                    else{
                        experience.setEnd(duration);
                    }
                }

                experienceList.add(experience);
            }
        }
        return experienceList;
    }

    private static List<Education> getPersonEducation(Set<Map<EducationSection, Object>> educationFieldMap) {
        List<Education> educationList = new ArrayList<>();
        try {
            if (null != educationFieldMap) {
                for (Map<EducationSection, Object> item : educationFieldMap) {
                    Education education = new Education();
                    String university = (String) item.get(EducationSection.UNIVERSITY);
                    String degree = (String) item.get(EducationSection.DEGREE);
                    String duration = (String) item.get(EducationSection.DURATION);


                    if (!util.isEmpty(university)) {
                        if (!university.contains("Other Qualifications")) {
                            education.setName(university.replace(",", ""));
                            if (!util.isEmpty(degree)) {
                                if (degree.contains("(")) {
                                    String dm[] = degree.split("\\(");
                                    education.setDegree(dm[0]);
                                }
                                String major = degree.substring(degree.indexOf("(") + 1, degree.indexOf(")"));
                                if (!util.isEmpty(major))
                                    education.setMajor(major);
                            }
                            if (!util.isEmpty(duration))
                                education.setEnd(duration);

                            educationList.add(education);
                        }
                    }
                }
            }
        }
        catch (Exception e){
            log.error("Exception in parsing:"+e.toString());
        }
        return educationList;
    }

    private static Map splitString(String input,String delimiter) {
        Map map = new HashMap();
        if (null != input) {
            String[] words = input.split(delimiter);
            map.put("left", words[0]);
            if (words.length > 1)
                map.put("right", words[1]);
            return map;
        }
        return null;
    }

    private static Name getPersonName(String name) {
        Name personName=new Name();

        if(!util.isEmpty(name)) {
            String[] names = name.split(" ", 2);
            personName.setGiven_name(names[0]);
            if(names.length>1)
                personName.setFamily_name(names[1]);
            return personName;
        }
        return null;
    }
   /* public static void main(String[] args) throws Exception {

        String fileContent=readFile("/Users/jenefar/workspace/marketplacedev/extensionservice/src/main/java/com/piqube/marketplace/parser/sites/naukri/test2.html");
        NaukriProfileParser parser=new NaukriProfileParser();
        if(null!=fileContent)
            parser.parsePerson(fileContent);
        else
            System.out.println("user is deactivated");

    }
    public static String readFile(String localFile) throws Exception {
        FileReader fileReader = new FileReader( localFile );
        BufferedReader reader = new BufferedReader(fileReader);
        StringWriter stringWriter = new StringWriter();
        String line;
        while( ( line = reader.readLine() ) != null )
            stringWriter.write(line);
        return stringWriter.toString();
    }*/
}

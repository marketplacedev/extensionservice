package com.piqube.marketplace.parser.sites.timesjobs;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.piqube.marketplace.parser.CrawledSource;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils.ParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;
import com.piqube.marketplace.parser.sites.linkedin.LinkedinParser;
import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.marketplace.parser.utils.UnaryFunction;
import com.piqube.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author jenefar
 */
public class TimesJobsProfileParser implements Parser {

    public static Logger log = LoggerFactory.getLogger(TimesJobsProfileParser.class);

    static FileUtil util =new FileUtil();
    public static final String monthsRegex = "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)";
    public static final String dateRegex =
            String.format( "(?i)(?<startMonth>%s)[\\s]+(?<startYear>[\\d]{4})[\\s]+to[\\s]+((?<endMonth>%s)[\\s]+(?<endYear>[\\d]{4})|(?<current>till date))", monthsRegex, monthsRegex );

    public  enum Sections implements IEnumValue<_SelectorMetaInfo> {

        NAME_SELECTOR(  new _SelectorMetaInfo(Arg.E(".profile-name") ) ),
        DESIGNATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".fields label:contains(Current Designation:) ~span") ) ),
        ROLE_SELECTOR( new _SelectorMetaInfo( Arg.E(".fields label:contains(Current Role:) ~span") ) ),

        LOCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".fields label:contains(Current Location:) ~span") ) ),
        PERSONAL_DETAILS_SELECTOR( new _SelectorMetaInfo( Arg.E(".contactDetails"),Arg.ownText() ) ),
        EMAIL_SELECTOR( new _SelectorMetaInfo( Arg.E(".profile-email a")) ) ,
        PHONE_SELECTOR( new _SelectorMetaInfo( Arg.E(".profile-email a ~a")) ) ,
        ADDRESS_SELECTOR( new _SelectorMetaInfo( Arg.E(".address")) ) ,
        INDUSTRY_SELECTOR( new _SelectorMetaInfo( Arg.E(".fields label:contains(Industry:) ~span") ) ),
        CURRENT_SALARY_SELECTOR( new _SelectorMetaInfo( Arg.E(".fields label:contains(Salary:) ~span") ) ),
        NOTICE_PERIOD_SELECTOR( new _SelectorMetaInfo( Arg.E(".fields label:contains(Notice Period:) ~span") ) ),

        FUNCTIONAL_AREA_SELECTOR( new _SelectorMetaInfo( Arg.E(".fields label:contains(Functional Area:) ~span") ) ),

        LANGUAGE_SELECTOR( new _SelectorMetaInfo( Arg.E(".lang-sect ul .langset"),Arg.list())),

        CURRENT_COMPANY( new _SelectorMetaInfo( Arg.E(".fields label:contains(Current Employer:) ~span") ) ),

        PROFILE_URL(new _SelectorMetaInfo( Arg.E("link[rel=canonical]"),Arg.a("href"))),
        KEY_SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E(".tj-ln-skills"),Arg.list()) ),


        EXPERIENCE_SELECTOR( new _SelectorMetaInfo(
                Arg.E(".fields label:contains(Previous Employer:) ~span"))),

        EDUCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".educationTitle ~p"),Arg.list(),
                Arg.Children(EducationSection.class) ) ),


        PROJECT_SELECTOR( new _SelectorMetaInfo( Arg.E(".project-details-box"),Arg.list(),
                Arg.Children(ProjectSection.class) ) );
        ;
        private _SelectorMetaInfo metaInfo;

        Sections(_SelectorMetaInfo info) {
            this.metaInfo = info;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }

    }

    public  enum LanguageSection implements IEnumValue<_SelectorMetaInfo> {

        LANGUAGE(new _SelectorMetaInfo( Arg.E(".cl ~li")) ),
        PROFICIENCY(new _SelectorMetaInfo( Arg.E(".cl ~li ~li")) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private LanguageSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public enum ExperienceSection implements IEnumValue<_SelectorMetaInfo> {

        DISPLAY( new _SelectorMetaInfo( Arg.E("label:contains(Previous Employer:) ~span") ) )

        ;
        public _SelectorMetaInfo metaInfo;
        private ExperienceSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {


        DISPLAY(new _SelectorMetaInfo( Arg.E("label:contains(Qualification) ~span") ) )

        ;
        public _SelectorMetaInfo metaInfo;
        private EducationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public  enum ProjectSection implements IEnumValue<_SelectorMetaInfo> {

        NAME(new _SelectorMetaInfo( Arg.E(".title")) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".time-box")) ),
        CLIENT(new _SelectorMetaInfo( Arg.E(".details span:contains(Client) ~span"), Arg.text() ) ),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".details span:contains(Role Description) ~span"), Arg.text() ) ),
        ROLE(new _SelectorMetaInfo( Arg.E(".details span:contains(Role) ~span"), Arg.text() ) ),
        EMPLOYMENT_NATURE(new _SelectorMetaInfo( Arg.E(".details span:contains(Employment Nature) ~span"), Arg.text() ) ),


        ;
        public _SelectorMetaInfo metaInfo;
        private ProjectSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }



    public static class MemberIdNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            if (input.contains("-")) {
                String[] str = input.split("-");
                if (str.length > 0)
                    return str[1].trim();
                return null;
            } else {
                return null;
            }
        }
    }
    @Override
    public Person parsePerson(String htmlSource) throws ParsingException {

        Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
        Set<Map.Entry<Sections, Object>> entries = map.entrySet();
        for (Map.Entry<Sections, Object> entry : entries) {
            log.debug(String.valueOf(entry.getKey()));
            log.debug(String.valueOf(entry.getValue()));
        }

        Set<Map<EducationSection, Object>> educationFieldMap = (Set<Map<EducationSection, Object>>) map.get(Sections.EDUCATION_SELECTOR);
        Set<Map<ProjectSection, Object>> projectFieldMap = (Set<Map<ProjectSection, Object>>) map.get(Sections.PROJECT_SELECTOR);

        Set<String> skills = (Set<String>) map.get(Sections.KEY_SKILLS_SELECTOR);

        String name = (String) map.get(Sections.NAME_SELECTOR);
        String location = (String) map.get(Sections.LOCATION_SELECTOR);
        String currentCompany = (String) map.get(Sections.CURRENT_COMPANY);
        String profileUrl = (String) map.get(Sections.PROFILE_URL);
        String currentDesignation = (String) map.get(Sections.DESIGNATION_SELECTOR);
        String currentRole = (String) map.get(Sections.ROLE_SELECTOR);

        String email = (String) map.get(Sections.EMAIL_SELECTOR);
        String address = (String) map.get(Sections.ADDRESS_SELECTOR);
        String industry = (String) map.get(Sections.INDUSTRY_SELECTOR);
        String functionalArea = (String) map.get(Sections.FUNCTIONAL_AREA_SELECTOR);
        String personalDetails = (String) map.get(Sections.PERSONAL_DETAILS_SELECTOR);
        String phone = (String) map.get(Sections.PHONE_SELECTOR);
        String currentSalary = (String) map.get(Sections.CURRENT_SALARY_SELECTOR);
        String noticePeriod = (String) map.get(Sections.NOTICE_PERIOD_SELECTOR);

        String previousExperience = (String) map.get(Sections.EXPERIENCE_SELECTOR);
        Set<String> languages=(Set)map.get(Sections.LANGUAGE_SELECTOR);

        Person person = new Person();

        if (!util.isEmpty(name)) {
            Name personName = getPersonName(name);
            person.setName(personName);
        }

        if(!util.isEmpty(profileUrl))
            person.setProfileUrl(profileUrl);


        if(!util.isEmpty(industry))
            person.setIndustry(industry);
        if(!util.isEmpty(currentDesignation)) {
            person.setDesignation(currentDesignation);
        }
        else{
            if(!util.isEmpty(currentRole))
                person.setDesignation(currentRole);
        }
        if(!util.isEmpty(currentCompany)) {
           if(currentCompany.contains(" from ")){
            String[] company = currentCompany.split("\\, from");
                person.setCurrentCompany(company[0]);}
            else if(!currentCompany.contains("Fresher"))
               person.setCurrentCompany(currentCompany);
        }
        if(!util.isEmpty(location))
            person.setLocality(location);
        if(!util.isEmpty(functionalArea))
            person.setFunction(functionalArea);
        if(!util.isEmpty(address)) {
            List<Address> addresses=new ArrayList<>();
            Address address1=new Address();
            address1.setDisplay(address);
            addresses.add(address1);
            person.setAddress(addresses);
        }

        if(!util.isEmpty(email))
            person.setPrimaryEmail(email);
        if(!util.isEmpty(phone))
            person.setPhoneNumber(phone);
        if(!util.isEmpty(languages)){
            languages.remove(Iterables.get(languages, 0));
            List<Language> languageList=new ArrayList<>();
            for(String language:languages){
                Language lang=new Language();
                lang.setName(language);
                languageList.add(lang);
            }
            person.setLanguages_known(languageList);
        }
        if(!util.isEmpty(noticePeriod)){
            if(!noticePeriod.contains("Not Disclosed")) {
                if(noticePeriod.contains("days")){
                    person.setNoticePeriod(noticePeriod.replace(" days",""));
                }
                else if(noticePeriod.contains("months")){
                    int noticePeriodInMonths= Integer.parseInt(noticePeriod.replace(" months",""));
                    log.debug("noticePeriodInMonths is:{}",noticePeriodInMonths);
                    int days=noticePeriodInMonths*30;
                    log.debug("noticePeriod in days is:{}",days);

                    person.setNoticePeriod(String.valueOf(days));
                }
            }

        }
        if(!util.isEmpty(skills)) {
            List skillList=getPersonSkills(skills);
            if(!util.isEmpty(skillList))
                person.setSkills(skillList);
        }
        if(!util.isEmpty(personalDetails)) {
            PersonalDetails pd=new PersonalDetails() ;
            if(personalDetails.contains("Male") || personalDetails.contains("Female")) {
                if(personalDetails.contains("Male"))
                    pd.setGender("Male");
                else if(personalDetails.contains("Female"))
                    pd.setGender("Female");
                Map mp = splitString(personalDetails, ",");
                Map ageMap=splitString((String) mp.get("right"),"yrs");
                String age=(String) ageMap.get("left");
                person.setAge(Integer.valueOf(age.trim()));
            }
            else {
                Map ageMap = splitString(personalDetails, "yrs");
                String age = (String) ageMap.get("left");
                person.setAge(Integer.valueOf(age.trim()));
            }
            person.setPersonalDetails(pd);
        }

        if (!util.isEmpty(educationFieldMap)) {
            List<Education> educationList = getPersonEducation(educationFieldMap);
            if(educationList.size()>0)
                person.setEducation(educationList);
        }
        if (!util.isEmpty(currentCompany)) {
            List<Experience> experienceList = getPersonExperience(currentCompany,currentDesignation,previousExperience);
            if(experienceList.size()>0)
                person.setExperience(experienceList);
        }

        if(!util.isEmpty(currentSalary)) {

            try {
                String salary = getNumber(currentSalary);
                if (null != salary && currentSalary.contains("lacs")) {
                    log.debug("salary is:::{}", salary);
                    float f = Float.valueOf(salary.trim()).floatValue();
                    float i = f * 100000;
                    String s = Float.toString(i);
                    String sal = s.replace(".0", "");
                    person.setCurrentSalary(sal);
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        //person.computeAndSetMobility();
        //person.computeAndSetStability();
        person.setSource(CrawledSource.TIMESJOBS.name());
        person.setCrawledDate(new Date());

        log.debug("Person:"+person.toJSON());
        return person;
    }

    public  String getNumber(String str) {
        String floatRegexp="([+-]?(\\d+\\.)?\\d+)";

        Pattern pattern = Pattern.compile(floatRegexp);
        Matcher matcher = pattern.matcher(str);
        String number = null;
        while (matcher.find()) {
            number = matcher.group(1);
        }
        return number;
    }
    private List getPersonSkills(Set<String> skills) {
        try {
            List<String> skillSet = new ArrayList<>();
           // List<String> skillLists = new ArrayList<>();

            Set<String> hashSet = new HashSet<String>();
            log.debug("skills:::::"+skills.toString());
            for (String s : skills) {
                log.debug("skill array:"+s);
                String[] values = s.split(",");
                List<String> skillLists  = Arrays.asList(values);
                for(String s1:skillLists) {
                    skillSet.add(s1);
                }
            }
            log.debug("skills:::::" + skills.toString());
            /*if (!util.isEmpty(hashSet)) {
                skillSet.addAll(hashSet);
            }*/
            Set<String> skill = new HashSet<>(skillSet);
            return Lists.newArrayList(skill);
        } catch (Exception e) {
            log.debug("skill parsing exception timesjobs:" + skills.toString());
            e.printStackTrace();
        }
        return null;
    }

    private List<Projects> getPersonProject(Set<Map<ProjectSection, Object>> projectFieldMap) {
        List<Projects> projectList=new ArrayList<>();
        if (null != projectFieldMap) {
            int experienceCount=0;
            for (Map<ProjectSection, Object> item : projectFieldMap) {
                Projects project=new Projects();

                String name = (String) item.get(ProjectSection.NAME);
                String client = (String) item.get(ProjectSection.CLIENT);
                String designation = (String) item.get(ProjectSection.ROLE);
                String duration=(String) item.get(ProjectSection.DURATION);
                String description=(String) item.get(ProjectSection.DESCRIPTION);
                String employmentNature=(String) item.get(ProjectSection.EMPLOYMENT_NATURE);


                if (!util.isEmpty(name))
                    project.setName(name);
                if (!util.isEmpty(client))
                    project.setClient(client);
                if (!util.isEmpty(employmentNature))
                    project.setEmploymentNature(employmentNature);
                if (!util.isEmpty(designation))
                    project.setRole(designation);
                if (!util.isEmpty(description))
                    project.setDesc(description);
                if (!util.isEmpty(duration)) {

                    if(duration.contains("Team Size  "))
                    {
                        Map m1=splitString(duration,"Team Size  ");
                        duration=m1.get("left").toString();
                        if(null !=m1.get("right"))
                            project.setTeamSize(Integer.valueOf(util.replaceUnicodeWithASCII(m1.get("right").toString())));
                    }
                    if (duration.contains(" to ")) {
                        Map map = splitString(duration, " to ");
                        project.setStart((String) map.get("left"));
                        if (map.get("right").toString().contains("Till Date")) {
                            project.setEnd("Present");
                            project.setCurrent(true);
                        } else {
                            project.setEnd(map.get("right").toString());
                        }
                    }
                    else{
                        project.setEnd(duration);
                    }
                }

                projectList.add(project);
            }
        }
        return projectList;

    }


    private static List<Experience> getPersonExperience(String currentCompany,String currentDesignation, String previousExperience) {
        List<Experience> experienceList=new ArrayList<>();
        List<String> previousExperiences = new ArrayList<>();
        log.debug("previous experience::::::::::::"+previousExperience);
        try{
        if(null !=previousExperience) {
            if(previousExperience.contains(":")) {
                String exp1 = previousExperience.substring(previousExperience.indexOf(":") + 1, previousExperience.indexOf("to ") + 12);
                log.debug("exp is:::::" + exp1);
                Map exp2 = splitString(previousExperience, exp1);
                log.debug("exp2 " + exp2.get("right"));
                while (!previousExperience.isEmpty()) {
                    String exp = previousExperience.substring(0, previousExperience.indexOf("to ") + 11);
                    if (exp.contains("Previous Employer:")) {
                        Map mp = splitString(exp, "Previous Employer:");
                        previousExperiences.add((String) mp.get("right"));
                    } else
                        previousExperiences.add(exp);
                    previousExperience = previousExperience.replace(exp, "");
                }
                log.debug("Experiences::::" + previousExperiences.toString());
            }
        }

            int experienceCount=0;
                Experience experience=new Experience();
                experience.setResumeOrdinal(experienceCount);

                if(currentCompany.contains(", from")) {
                    String duration = currentCompany.substring(currentCompany.indexOf(", from") + 6, currentCompany.indexOf("Onwards"));
                    //experience.setDuration(duration.trim());
                    String[] company = currentCompany.split("\\, from");
                    experience.setOrg(company[0]);
                    experience.setCurrent(true);
                    experience.setStart(duration.trim());
                    if(!util.isEmpty(currentDesignation))
                        experience.setTitle(currentDesignation);
                    experienceList.add(experience);

                }
                else if(!currentCompany.contains("Fresher")){
                    experience.setOrg(currentCompany);
                    experience.setCurrent(true);
                    experienceList.add(experience);

                }

            if(previousExperiences.size()>0){
                for(String previousExp:previousExperiences){
                    Experience exper=new Experience();
                    experienceCount=experienceCount+1;
                    exper.setResumeOrdinal(experienceCount);
                    log.debug("previous exp:((((((("+previousExp);
                    Map cm=splitString(previousExp," as ");
                    exper.setOrg((String) cm.get("left"));
                    Map dm=splitString((String) cm.get("right")," from ");
                    exper.setTitle((String) dm.get("left"));
                    experienceList.add(exper);
                    Map duration=splitString((String) dm.get("right")," to ");
                    exper.setStart((String) duration.get("left"));
                    exper.setEnd((String) duration.get("right"));

                }

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return experienceList;
    }

    private static List<Education> getPersonEducation(Set<Map<EducationSection, Object>> educationFieldMap) {
        List<Education> educationList = new ArrayList<>();
        try {
            if (null != educationFieldMap) {
                for (Map<EducationSection, Object> item : educationFieldMap) {
                    Education education = new Education();
                    String display = (String) item.get(EducationSection.DISPLAY);
                    if(!display.contains("Not Specified")) {
                        Map map = splitString(display, "»");
                        String degreeText = (String) map.get("left");
                        if (degreeText.contains(" in ")) {
                            String[] degree = degreeText.split(" in ");
                            education.setDegree(degree[0]);
                            if (degree.length > 1)
                                education.setMajor(degree[1]);
                        }
                        log.debug("right is:::" + (String) map.get("right"));
                        if (null != map.get("right")) {
                            Map ump = splitString((String) map.get("right"), "»");
                            String university = (String) ump.get("left");
                            education.setName(university);
                        }
                        if(display.contains("Year ")) {
                            String[] year = display.split("Year ");
                            education.setEnd(year[1]);
                        }

                        if(null!=education.getDegree())
                            educationList.add(education);
                    }
                }
            }
        }
        catch (Exception e){
            log.error("Exception in parsing:"+e.toString());
            e.printStackTrace();
        }
        return educationList;
    }

    private static Map splitString(String input,String delimiter) {
        Map map = new HashMap();
        if (null != input) {
            String[] words = input.split(delimiter);
            map.put("left", words[0]);
            if (words.length > 1)
                map.put("right", words[1]);
            return map;
        }
        return null;
    }

    private static Name getPersonName(String name) {
        Name personName=new Name();

        if(!util.isEmpty(name)) {
            String[] names = name.split(" ", 2);
            personName.setGiven_name(names[0]);
            if(names.length>1)
                personName.setFamily_name(names[1]);
            return personName;
        }
        return null;
    }

/*  public static void main(String[] args) throws Exception {

        String fileContent=readFile("/Users/jenefar/workspace/marketplacedev/extensionservice/src/main/java/com/piqube/marketplace/parser/sites/timesjobs/test7.html");
        TimesJobsProfileParser parser=new TimesJobsProfileParser();
        if(null!=fileContent)
            parser.parsePerson(fileContent);

    }*/
    public static String readFile(String localFile) throws Exception {
        FileReader fileReader = new FileReader( localFile );
        BufferedReader reader = new BufferedReader(fileReader);
        StringWriter stringWriter = new StringWriter();
        String line;
        while( ( line = reader.readLine() ) != null )
            stringWriter.write(line);
        return stringWriter.toString();
    }
}
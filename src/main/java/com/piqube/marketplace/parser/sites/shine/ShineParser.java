package com.piqube.marketplace.parser.sites.shine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.Set;


import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.piqube.marketplace.parser.CrawledSource;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;

import com.piqube.marketplace.parser.sites.linkedin.LinkedinParser;

import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.model.DateOfBirth;
import com.piqube.model.Education;
import com.piqube.model.Experience;
import com.piqube.model.Language;
import com.piqube.model.Name;
import com.piqube.model.Person;
import com.piqube.model.PersonalDetails;


public class ShineParser implements Parser {
	public static Logger log = LoggerFactory.getLogger(LinkedinParser.class);

	static FileUtil util = new FileUtil();
	List<Language> allLanguages = new ArrayList<Language>();

	public enum Sections implements IEnumValue<_SelectorMetaInfo> {

		NAME_SELECTOR(new _SelectorMetaInfo(Arg.E("#shine_profile_top .linkdnprofile_name"))),
		POSITION(new _SelectorMetaInfo(Arg.E("#shine_profile_top .linkdnprofile_skills"))), 
		DATE(new _SelectorMetaInfo(Arg.E("#shine_profile_top .linkdnprofile_date"))), 
		
		LIST(new _SelectorMetaInfo(Arg.E("#shineprofile .inner_box"), Arg.list())),
								
		JOB_DETAILS(new _SelectorMetaInfo(Arg.E("div.education_box div.inner_box"), Arg.list())), 
		EDUCATION_DETAILS(new _SelectorMetaInfo(Arg.E("div.education_box.eb2 div.inner_box"),Arg.list())), 
		SKILLS(new _SelectorMetaInfo(Arg.E("div.resume_box div.pro_btn"),Arg.list())), 
		CERTIFICATIONS(new _SelectorMetaInfo(Arg.E("div.Certifications_box > ul"),Arg.list())), 
		OTHERDETAILS(new _SelectorMetaInfo(Arg.E("div.resume_box > ul"), Arg.list())),

		;

		private _SelectorMetaInfo metaInfo;

		Sections(_SelectorMetaInfo info) {
			this.metaInfo = info;
		}

		@Override
		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}

	public enum CTCSection implements IEnumValue<_SelectorMetaInfo> {

		CTC(new _SelectorMetaInfo(Arg.E("u:contains(CTC) ~em"), Arg.text())),

		;
		public _SelectorMetaInfo metaInfo;

		private CTCSection(_SelectorMetaInfo metaInfo) {
			this.metaInfo = metaInfo;
		}

		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}

	@Override
	public Person parsePerson(String htmlSource) throws ParsingException {

		Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
		Set<Map.Entry<Sections, Object>> entries = map.entrySet();
		for (Map.Entry<Sections, Object> entry : entries) {
			System.out.println(entry.getKey());
			System.out.println(entry.getValue());
			System.out.println("\n");

		}

		String name = (String) map.get(Sections.NAME_SELECTOR);

		String date = (String) map.get(Sections.DATE);

		Set<String> list = (Set<String>) map.get(Sections.LIST);

		String position = (String) map.get(Sections.POSITION);

		Set<String> jobDetails = (Set<String>) map.get(Sections.JOB_DETAILS);

		Set<String> otherDetails = (Set<String>) map.get(Sections.OTHERDETAILS);

		Set<String> education = (Set<String>) map.get(Sections.EDUCATION_DETAILS);

		Set<String> skills = (Set<String>) map.get(Sections.SKILLS);

//		Set<String> ctcData = (Set<String>) map.get(Sections.CTC_SELECTOR);

		List<String> emails = new ArrayList<String>();

		Person person = new Person();
		PersonalDetails personalDetails = new PersonalDetails();
		DateOfBirth dateOfBirth = new DateOfBirth();
		
		//name
		if (!util.isEmpty(name)) {
			Name personName = getPersonName(name);
			person.setName(personName);

		}
		
		//lastseen
		if (!util.isEmpty(date)) {
			String[] names = date.split(" ", 5);

			person.setLastSeen(names[3].trim());

		}
		
		//company
		if (!util.isEmpty(list)) {
			String currentCompany = list.toString();
			String company = StringUtils.substringBetween(currentCompany.trim(), "Company:", "Job Title:");
			if (company.trim().equals("Not Mentioned")) {

			} else {

				person.setCurrentCompany(company.trim());
			}
		}
		
		//designation
		if (!util.isEmpty(list)) {

			String designation = list.toString();
			String jobTitle = StringUtils.substringBetween(designation.trim(), "Job Title:", "CTC:");

			if (jobTitle.trim().equals("Not Mentioned")) {

			} else {
			person.setDesignation(jobTitle.trim());
			}
		}
		
        //ctc		
		if (!util.isEmpty(list)) {
			String ctc = list.toString();
			String ctcPerson = StringUtils.substringBetween(ctc.trim(), "CTC:", "Total Experience:");
			if (ctcPerson.trim().equals("Not Mentioned")) {

			} else {
				if(ctcPerson.trim().endsWith("Lacs")){
				String remove=ctcPerson.trim().replace("Rs.", "");
				String removeData=remove.trim().replace("Lacs", "");
				String removePerson=removeData.trim().replace("Lac", "");
				
				float f = Float.valueOf(removePerson.trim()).floatValue();
				float i=f*100000;
				String s = Float.toString(i);
				String salary=s.replace(".0", "");
			person.setCurrentSalary(salary);
				}else{
					String remove=ctcPerson.trim().replace("Rs.", "");
					String removeData=remove.trim().replace("Thousand", "");
					String removePerson=removeData.trim().replace("Thousands", "");
					
					float f = Float.valueOf(removePerson.trim()).floatValue();
					float i=f*1000;
					String s = Float.toString(i);
					String salary=s.replace(".0", "");
					person.setCurrentSalary(salary);
				}
				
			}
		}
		
        //noticePeriod		
		if (!util.isEmpty(list)) {
			String noticePeriod = list.toString();
			String noticePeriodPerson = StringUtils.substringBetween(noticePeriod.trim(), "Notice Period:", "Email:");
			String data = noticePeriodPerson.replace(",", "");
			if (data.trim().equals("Not Mentioned")) {

			} else {
			
				if(data.trim().endsWith("week")){
					String remove=data.trim().replace("week", "");
					
					String noti=remove.trim().replace("<", "");
					float f = Float.valueOf(noti.trim()).floatValue();
					float i=f*7;
					String s = Float.toString(i);
					String noticePeriodData=s.replace(".0", "");
					person.setNoticePeriod(noticePeriodData);
				}
				if(data.trim().endsWith("weeks")){
					String remove=data.trim().replace("weeks", "");
					
					String noti=remove.trim().replace("<", "");
					float f = Float.valueOf(noti.trim()).floatValue();
					float i=f*7;
					String s = Float.toString(i);
					String noticePeriodData=s.replace(".0", "");
					person.setNoticePeriod(noticePeriodData);
				}
				if(data.trim().endsWith("months")){
					String remove=data.trim().replace("months", "");
					
					String noti=remove.trim().replace("<", "");
					float f = Float.valueOf(noti.trim()).floatValue();
					float i=f*30;
					String s = Float.toString(i);
					String noticePeriodData=s.replace(".0", "");
					person.setNoticePeriod(noticePeriodData);
				
				}	
				if(data.trim().endsWith("month")){
					String remove=data.trim().replace("month", "");
					
					String noti=remove.trim().replace("<", "");
					float f = Float.valueOf(noti.trim()).floatValue();
					float i=f*30;
					String s = Float.toString(i);
					String noticePeriodData=s.replace(".0", "");
					person.setNoticePeriod(noticePeriodData);
				
				}	
			}	
			
		}
		
		
        //primaryEmail
		if (!util.isEmpty(list)) {
			String email = list.toString();
			String emailId = StringUtils.substringBetween(email.trim(), "Email:", "Contact No:");
			if (emailId.trim().equals("Not Mentioned")) {

			} else {
			person.setPrimaryEmail(emailId.trim());
		}
		}
		
		//phoneNumber
		if (!util.isEmpty(list)) {
			String mobile = list.toString();
			String phoneNum = StringUtils.substringBetween(mobile.trim(), "Contact No:", "Location:");
			if (phoneNum.trim().equals("Not Mentioned")) {

			} else {
			String phoneNumber = null;
			String[] mobileArray = phoneNum.split("-");
			if (mobileArray.length >= 2) {
				phoneNumber = mobileArray[1];
				phoneNumber = phoneNumber.substring(0, 10);
				person.setPhoneNumber(phoneNumber.trim());
			}else{
				person.setPhoneNumber(phoneNum.trim());
			}

			
		}
		}
		
		//location
		if (!util.isEmpty(list)) {
			String locality = list.toString();
			String location = StringUtils.substringBetween(locality.trim(), "Location:", "Gender:");
			if (location.trim().equals("Not Mentioned")) {

			} else {
			person.setLocality(location.trim());
			}
		}

		//gender
		if (!util.isEmpty(list)) {
			String gender = list.toString();
			String genderPerson = StringUtils.substringBetween(gender.trim(), "Gender:", "Date Of Birth:");
			if (genderPerson.trim().equals("Not Mentioned")) {

			} else {
			personalDetails.setGender(genderPerson.trim());
			person.setPersonalDetails(personalDetails);
		}
		}
		
        //dob
		if (!util.isEmpty(list)) {
			String dob = list.toString();
			String dobPerson = StringUtils.substringBetween(dob.trim(), "Date Of Birth:", ",");
			String dataPerson = dobPerson.replace(",", "");
			if (dataPerson.trim().equals("Not Mentioned")) {

			} else {
			
			String[] data = dataPerson.split(" ");

			dateOfBirth.setDay(data[0]);
			dateOfBirth.setMonth(data[1]);
			dateOfBirth.setYear(data[2]);

			personalDetails.setDateOfBirth(dateOfBirth);

			person.setPersonalDetails(personalDetails);
		}
		}
		
		//Education
		if (!util.isEmpty(education)) {
			List<Education> educationList = new ArrayList<Education>();
			for (String educat : education) {
				Education educati = new Education();
				String eduData = educat.toString();

				String insName = StringUtils.substringBetween(eduData.trim(), "Institute Name:", "Course Type:");
				String degree = StringUtils.substringBetween(eduData.trim(), "Qualification level:",
						"Education Stream:");
				String major = StringUtils.substringBetween(eduData.trim(), "Education Stream:", "Institute Name:");
				String end[] = eduData.split(":");

				educati.setName(insName.trim());
				educati.setDegree(degree.trim());
				educati.setMajor(major.trim());
				educati.setEnd(end[5].trim());

				educationList.add(educati);

			}
			person.setEducation(educationList);
		}

		int experienceCount=0;

		//Experience
		if (!util.isEmpty(jobDetails)) {
			List<Experience> experienceList = new ArrayList<Experience>();
			for (String jobDetail : jobDetails) {
				Experience experience = new Experience();

				String jobDeta = jobDetail.toString();
				if (jobDeta.contains("Qualification level:")) {

				} else {

					String jobtitle = StringUtils.substringBetween(jobDeta.trim(), "Job Title:", "Functional Area");
					String company = StringUtils.substringBetween(jobDeta.trim(), "Company:", "Years in the job:");
					String duration[] = jobDeta.split(":");

					experience.setTitle(jobtitle.trim());
					experience.setOrg(company.trim());
					experience.setDuration(duration[5].trim());
					experience.setResumeOrdinal(experienceCount);
					experienceList.add(experience);
					experienceCount=experienceCount+1;
				}

			}
			person.setExperience(experienceList);
		}

		
		//Skills
		if (!util.isEmpty(skills)) {

			String[] skill = skills.toString().split(",");
			List<String> skillData = new ArrayList<String>();

			String skillPerson;
			String dataSkill;
			for (String string : skill) {

				skillPerson = string.replace("[", "");
				dataSkill = skillPerson.replace("]", "");
				String digits = dataSkill.replaceAll("[0-9]", "");
				String digitData = digits.replace("Yrs", "");
				String digitsPerson = digitData.replaceAll("Yr", "");
				String symbolPerson = digitsPerson.replaceAll("<", "");
				String personSkills = symbolPerson;
				skillData.add(personSkills.trim());

			}
			person.setSkills(skillData);

		}
        
		//Preferred_locations
		if (!util.isEmpty(otherDetails)) {
			List<String> preferred_locations = new ArrayList<>();

			String otherDetail = otherDetails.toString();
			String jobLocation = StringUtils.substringBetween(otherDetail, "Job Location", "Functional Area");

			String[] names = jobLocation.trim().split("\\|");
			for (String string : names) {
				String preferredJoblocations = null;
				preferredJoblocations = string.trim();
				preferred_locations.add(preferredJoblocations);
			}

			person.setPreferred_locations(preferred_locations);
		}

		person.setSource(CrawledSource.SHINE.name());

		System.out.println("\n\n\n\n\n\n" + "Person::::" + person.toJSON());
		return person;
	}

	private static Name getPersonName(String name) {
		Name personName = new Name();
		try {
			if (!util.isEmpty(name)) {
				String[] names = name.split(" ", 2);
				personName.setGiven_name(util.replaceUnicodeWithASCII(names[0].replace(",", "").trim()));

				if (names.length > 1)
					personName.setFamily_name(util.replaceUnicodeWithASCII(names[1].replace(",", "").trim()));

				return personName;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*	public static void main(String[] args) throws Exception {
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/Dharmesh Meena _ Deputy General Manager - Marketing, Askmebazaar.com.html");
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/shinedata.htm");
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/Suresh Kumar _ Delivery Manager, MetricStream Infotech.html");
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/Aishwarya Tiwari _ Junior .Net Developer, SSPL.html");
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/Anupam Kumar _ Java Developer, xenonstack-A stack innovator.html");
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/Ashish Prasad _ Marketing Manager, Shalimar Wires Industries Ltd.html");
		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/Bani Das _ Sr Associate - Projects, Cognizant.html");
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/Dhaval J. Pandya _ .Net Developer, xipra technology.html");
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/Fatima Noori _ Not Applicable.html");
//		String fileContent = readFile("C:/Users/acer lap/Downloads/shine new html/ARUNACHALAM M _ Software Engineer, Attune Technologies.html");
		ShineParser parser = new ShineParser();
		if (null != fileContent)
			parser.parsePerson(fileContent);
		else
			System.out.println("user is deactivated");
	}

	public static String readFile(String localFile) throws Exception {
		FileReader fileReader = new FileReader(localFile);
		BufferedReader reader = new BufferedReader(fileReader);
		StringWriter stringWriter = new StringWriter();
		String line;
		while ((line = reader.readLine()) != null)
			stringWriter.write(line);
		return stringWriter.toString();
	}*/

}

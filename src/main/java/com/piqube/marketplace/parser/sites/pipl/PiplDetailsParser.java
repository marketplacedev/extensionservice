package com.piqube.marketplace.parser.sites.pipl;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.piqube.model.PersonContact;
import com.piqube.model.SocialProfiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.Map;
import java.util.Set;

/**
 * @author jenefar
 */
public class PiplDetailsParser {
    /*public static Logger log = LoggerFactory.getLogger(PiplDetailsParser.class);

    static FileUtil fileUtil = new FileUtil();

    private String mongo_host;
    private String mongo_db;
    public String mongo_collection;
    public Mongo mongo;
    public MongoTemplate mongoTemplate;
    public MongoClient mongoClient;
    public DB mongoDB;


    public PiplDetailsParser(){
        this.mongo_host= PiqubeProperties.getValue("target.mongo.host");
        this.mongo_db=PiqubeProperties.getValue("target.mongo.db");
        this.mongo_collection=PiqubeProperties.getValue("target.mongo.collection");
        try {
            mongo = new Mongo(mongo_host);
            mongoTemplate = new MongoTemplate(mongo, mongo_db);
            mongoClient=new MongoClient(mongo_host);
            mongoDB=mongoClient.getDB(mongo_collection);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public PersonContact parsePerson(String htmlSource) throws ParsingException {
        Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
        String name = (String) map.get(Sections.NAME_SELECTOR);
        Set<String> links = (Set) map.get(Sections.LINKS_SELECTOR);

        System.out.println("Name:" + name);
        System.out.println("Links: " + links);
        PersonContact personContact = new PersonContact();
        SocialProfiles socialProfiles = new SocialProfiles();
        personContact.setName(name);
        if(null!=links) {
            for (String link : links)
                socialProfiles.addSocialLink(link);
        }
        personContact.setSocialProfiles(socialProfiles);


        return personContact;
    }

    public enum Sections implements IEnumValue<_SelectorMetaInfo> {


        NAME_SELECTOR(new _SelectorMetaInfo(Arg.E("#profile_summary .header") ,Arg.F(new NameNormalizer()))),
        LINKS_SELECTOR(new _SelectorMetaInfo(Arg.E(".person_result .person_content .line1"),Arg.list())),
                ;

        private _SelectorMetaInfo metaInfo;

        Sections(_SelectorMetaInfo info) {
            this.metaInfo = info;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }

    }
    public static class NameNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            try {
                if(input.contains("(")) {
                    String[] words = input.split("\\(");
                    return words[0];
                }
                return input;
            } catch (Exception e) {
                return null;
            }
        }
    }
    public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {

        DURATION(new _SelectorMetaInfo(Arg.E(".pro-txt"))),;
        public _SelectorMetaInfo metaInfo;

        private EducationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public static void main(String[] args) throws Exception {

        try {
            String q = "jayadev.m@gmail.com";
            String url = "https://pipl.com/search/?q=" + URLEncoder.encode(q, "UTF-8");
            System.out.println("url:" + url);
            PiplDetailsParser parser = new PiplDetailsParser();
            StringBuffer htmlSource = parser.sendRequest(url);

            if (null != htmlSource && !htmlSource.toString().contains("No Result Found for")) {
                PersonContact personContact = parser.parsePerson(htmlSource.toString());
                System.out.println(personContact.toString());
            }

        } catch (ParsingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    public static StringBuffer sendRequest(String url) {
        URL obj;
        HttpURLConnection con;
        BufferedReader in;
        StringBuffer response = null;
        try {
            obj = new URL(url);
            con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Testing");
            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //print result
           // System.out.println(response.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            return response;
        }
        return response;
    }*/
}

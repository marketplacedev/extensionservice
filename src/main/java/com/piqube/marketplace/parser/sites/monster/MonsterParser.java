package com.piqube.marketplace.parser.sites.monster;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.piqube.marketplace.parser.CrawledSource;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;
import com.piqube.marketplace.parser.parserutils.parsehelpers.NameValueSplit;
import com.piqube.marketplace.parser.sites.linkedin.LinkedinParser;
import com.piqube.marketplace.parser.sites.linkedin.LinkedinParser.EducationSection;
import com.piqube.marketplace.parser.sites.linkedin.LinkedinParser.TestScoresSection;
import com.piqube.marketplace.parser.sites.naukri.NaukriProfileParser.ExperienceSection;
import com.piqube.marketplace.parser.sites.naukri.NaukriProfileParser.Sections;
import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.model.DateOfBirth;
import com.piqube.model.Education;
import com.piqube.model.Experience;
import com.piqube.model.Language;
import com.piqube.model.Name;
import com.piqube.model.Person;
import com.piqube.model.PersonalDetails;
import com.piqube.model.TestScore;

public class MonsterParser implements Parser
{
	public static Logger log = LoggerFactory.getLogger(LinkedinParser.class);

	static FileUtil util =new FileUtil();
	List<Language> allLanguages = new ArrayList<Language>();

	public  enum Sections implements IEnumValue<_SelectorMetaInfo> {

		NAME_SELECTOR( new _SelectorMetaInfo( Arg.E(".skname") ) ),
		LOCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".info_loc ") ) ),
		CURRENT_DESIGNATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".skinfo") ) ),
		OTHER_BASIC_INFO( new _SelectorMetaInfo( Arg.E(".skr_basicinfo_other") ) ),
		PHONE_NUMBER( new _SelectorMetaInfo( Arg.E(".mob_container") ) ),
		DETAIL_SECTION( new _SelectorMetaInfo( Arg.E(".crinfo .scndinfo"),Arg.list(),Arg.Children(DetailsSection.class) ) ),
		HIGHEST_EDUCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".crinfo .scndinfo:contains(Education)"),Arg.text(),Arg.F(new NameValueSplit("Education:")))),
		SECOND_HIGHEST_EDUCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".crinfo .scndinfo:contains(Education) ~div"),Arg.text())),

		NOTICE_PERIOD( new _SelectorMetaInfo( Arg.E(".bottomboxtxt") ) ),
		SKILL_RECENCY( new _SelectorMetaInfo( Arg.E("div[class=skr_resaccord skr_skills active]  ~div .hg_mtch") ,Arg.list()) ),
		WORK_HISTORY( new _SelectorMetaInfo( Arg.E("div[class=skr_resaccord skr_work active]:contains(Work History) ~div"),Arg.elem(),Arg.Children(ExperienceSection.class) )) ,;

		private _SelectorMetaInfo metaInfo;

		Sections(_SelectorMetaInfo info) {
			this.metaInfo = info;
		}

		@Override
		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}

	public enum DetailsSection implements IEnumValue<_SelectorMetaInfo> {
		WORK_EXPERIENCE(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Work Experience:")))),
		SKILLS(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Skills:")))),
		DOMAIN(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Domain Knowledge:")))),
		INDUSTRY(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Industry:")))),
		CATEGORY(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Category:")))),
		ROLES(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Roles:")))),
		//EDUCATION(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.list(),Arg.Children(EducationSection.class))),
		JOB_LOCATIONS(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Preferred Job Location:")))),
		JOB_TYPE(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Job Type:")))),
		WORK_AUTH(new _SelectorMetaInfo( Arg.E(".scndinfo"),Arg.text(),Arg.F(new NameValueSplit("Work Authorization:")))),
		;


		public _SelectorMetaInfo metaInfo;
		private DetailsSection(_SelectorMetaInfo metaInfo) {
			this.metaInfo = metaInfo;
		}
		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}



	public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {
		EDUCATION_DETAILS_SECTION( new _SelectorMetaInfo( Arg.E(".scndinfo mrgn hg_mtch"),Arg.list()));//,Arg.Children(ExperienceDetailsSection.class) ) ),

		;


		public _SelectorMetaInfo metaInfo;
		private EducationSection(_SelectorMetaInfo metaInfo) {
			this.metaInfo = metaInfo;
		}
		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}


	public enum ExperienceSection implements IEnumValue<_SelectorMetaInfo> {
		EXPERIENCE_DETAILS_SECTION( new _SelectorMetaInfo( Arg.E(".skrworkrow"),Arg.list(),Arg.Children(ExperienceDetailsSection.class) ) ),

		;


		public _SelectorMetaInfo metaInfo;
		private ExperienceSection(_SelectorMetaInfo metaInfo) {
			this.metaInfo = metaInfo;
		}
		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}
	public enum ExperienceDetailsSection implements IEnumValue<_SelectorMetaInfo> {

		DESIGNATION(new _SelectorMetaInfo( Arg.E(".skrworkcell.hg_mtch.rle_mtch") ) ),
		COMPANY(new _SelectorMetaInfo( Arg.E("div[class=skrworkcell hg_mtch]") ) ),

		DURATION(new _SelectorMetaInfo( Arg.E(".skrworkcell.hg_mtch.rle_mtch ~.skrworkcell"))),
		END_DURATION(new _SelectorMetaInfo( Arg.E(".skrworkcell.hg_mtch.rle_mtch ~.skrworkcell > strong"))),
		CTC(new _SelectorMetaInfo( Arg.E(".skrworkcell.hg_mtch.rle_mtch ~.skrworkcell ~.skrworkcell"))),
		;


		public _SelectorMetaInfo metaInfo;
		private ExperienceDetailsSection(_SelectorMetaInfo metaInfo) {
			this.metaInfo = metaInfo;
		}
		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}


	@Override
	public Person parsePerson(String htmlSource) throws ParsingException {

		Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
		Set<Map.Entry<Sections, Object>> entries = map.entrySet();
		for (Map.Entry<Sections, Object> entry : entries) {
			System.out.println(entry.getKey());
			System.out.println(entry.getValue());
			System.out.println("\n");

		}


		String name = (String) map.get(Sections.NAME_SELECTOR);
		String location = (String) map.get(Sections.LOCATION_SELECTOR);
		String designationSection = (String) map.get(Sections.CURRENT_DESIGNATION_SELECTOR);
		String phoneNumber = (String) map.get(Sections.PHONE_NUMBER);
		String otherbasicInfoSection =(String) map.get(Sections.OTHER_BASIC_INFO);
		String noticePeriod =(String) map.get(Sections.NOTICE_PERIOD);
		Set<String> ITSkillSet=(Set<String>)map.get(Sections.SKILL_RECENCY);
		String highestEdu = (String) map.get(Sections.HIGHEST_EDUCATION_SELECTOR);
		String secondHighestEdu = (String) map.get(Sections.SECOND_HIGHEST_EDUCATION_SELECTOR);


		Set <String> newITSkillSet = new HashSet <String>();
		if(!util.isEmpty(ITSkillSet))
		{
			Iterator iterator = ITSkillSet.iterator();

			while (iterator.hasNext())
			{
				newITSkillSet.add(iterator.next().toString().toLowerCase());  
			}
		}

		String[] designation = null;
		String primaryEmail = null;
		List<String> emails = new ArrayList<String>();

		Map<ExperienceSection, Object> workHistoryMap = (Map<ExperienceSection, Object>) map.get(Sections.WORK_HISTORY);


		Set<Map<ExperienceSection, Object>> experienceFieldMap = (Set<Map<ExperienceSection, Object>>) map.get(ExperienceSection.EXPERIENCE_DETAILS_SECTION);
		Set<Map<DetailsSection, Object>> detailSectionFieldMap = (Set<Map<DetailsSection, Object>>) map.get(Sections.DETAIL_SECTION);

		//extracting emails and current desig
		if(!util.isEmpty(designationSection) && designationSection.contains(" at "))
		{
			designation = designationSection.split(" at ");

			Pattern p = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b",
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = p.matcher(designation[1]);

			while(matcher.find()) {
				emails.add(matcher.group());
			}
			if(emails.size()>0){
				primaryEmail = emails.get(0);
			}
		}else if(util.isEmpty(designationSection))
		{
			//do nothing
		}
		else{
			Pattern p = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b",
					Pattern.CASE_INSENSITIVE);
			Matcher matcher = p.matcher(designationSection);
			while(matcher.find()) {
				emails.add(matcher.group());
			}
			if(emails.size()>0){
				primaryEmail = emails.get(0);
			}
		}




		Person person = new Person();
		PersonalDetails personalDetails=getPersonBasicInfo(otherbasicInfoSection);

		if(!util.isEmpty(personalDetails))
			person.setPersonalDetails(personalDetails);

		if(!util.isEmpty(allLanguages))
			person.setLanguages_known(allLanguages);

		if (!util.isEmpty(workHistoryMap)) {
			List<Experience> experienceList = getPersonExperience(workHistoryMap);
			person.setExperience(experienceList);

		}
		if (!util.isEmpty(name)) {
			Name personName = getPersonName(name);
			person.setName(personName);
		}
		if(!util.isEmpty(location))
			person.setLocality(util.replaceUnicodeWithASCII(location.replace(" ", "").trim()));

		if(!util.isEmpty(phoneNumber)){
			person.setPhoneNumber(util.replaceUnicodeWithASCII(phoneNumber.replace(" ", "").trim().replace("-","")));
		}
		if(!util.isEmpty(primaryEmail)){
			person.setPrimaryEmail(util.replaceUnicodeWithASCII(primaryEmail.replace(" ", "").trim()));
		}
		if(!util.isEmpty(noticePeriod)){
			String[] noticePer = noticePeriod.split(":");
			person.setNoticePeriod(util.replaceUnicodeWithASCII(noticePer[1].replace(" ", "").trim()));
		}
		if (!util.isEmpty(detailSectionFieldMap))
		{
			for (Map<DetailsSection, Object> item : detailSectionFieldMap) {
				String workExperience = (String) item.get(DetailsSection.WORK_EXPERIENCE);
				String skills = (String) item.get(DetailsSection.SKILLS);
				String domain = (String) item.get(DetailsSection.DOMAIN);
				String Industry = (String) item.get(DetailsSection.INDUSTRY);
				String category = (String) item.get(DetailsSection.CATEGORY);
				String roles = (String) item.get(DetailsSection.ROLES);
				String jobLocation = (String) item.get(DetailsSection.JOB_LOCATIONS);
				String jobType = (String) item.get(DetailsSection.JOB_TYPE);
				String workAuth = (String) item.get(DetailsSection.WORK_AUTH);
				HashSet<String> emptyITSkillSet = new HashSet<String>();

				if(!util.isEmpty(workHistoryMap))
				{
					Set<Map<ExperienceDetailsSection,Object>> experienceDetailsSectionObjectMap= (Set<Map<ExperienceDetailsSection, Object>>) workHistoryMap.get(ExperienceSection.EXPERIENCE_DETAILS_SECTION);
					List<String> allCTC= new ArrayList<String>();

					for (Map<ExperienceDetailsSection, Object> thing : experienceDetailsSectionObjectMap) {

						String ctc=(String) thing.get(ExperienceDetailsSection.CTC);

						if (!util.isEmpty(ctc))
						{
							allCTC.add(ctc);
							if(allCTC.size()>0)
							{
								person.setCurrentSalary(allCTC.get(0).replace(" ", "").trim());
							}

						}	
					}
				}
				if(!util.isEmpty(Industry))
				{
					person.setIndustry(util.replaceUnicodeWithASCII(Industry.replace(" ", "").trim()));
				}
				if(!util.isEmpty(roles))
				{
					person.setRole(util.replaceUnicodeWithASCII(roles.replace(" ", "").trim()));
				}

				if(!util.isEmpty(jobLocation))
				{	
					List<String> prefLocations = new ArrayList<>();
					if(jobLocation.contains(","))
					{
						String[] prefJobLoc = jobLocation.split(",");
						for(int i=0;i<prefJobLoc.length;i++)
						{
							prefLocations.add(util.replaceUnicodeWithASCII(prefJobLoc[i].replace(" ", "").trim()));
						}
					}else{
						prefLocations.add(util.replaceUnicodeWithASCII(jobLocation.replace(" ", "").trim()));
					}
					person.setPreferred_locations(prefLocations);
				}

				if(!util.isEmpty(skills))
				{
					if(skills.contains(","))
					{
						String[] tabSkills = skills.split(",");
						for(int i=0;i<tabSkills.length;i++)
						{
							if(!util.isEmpty(newITSkillSet) && !newITSkillSet.contains(tabSkills[i]))
							{
								newITSkillSet.add(util.replaceUnicodeWithASCII(tabSkills[i].replace(" ", "").trim().toLowerCase()));
								person.setSkills(Lists.newArrayList(newITSkillSet));
							}else if(util.isEmpty(newITSkillSet) && tabSkills[i]!=null)
							{
								emptyITSkillSet.add(util.replaceUnicodeWithASCII(tabSkills[i].trim().toLowerCase()));
								person.setSkills(Lists.newArrayList(emptyITSkillSet));
							}
						}
					}
					else
					{
						if(!util.isEmpty(newITSkillSet))
						{
							newITSkillSet.add(util.replaceUnicodeWithASCII(skills.replace(" ", "").trim().toLowerCase()));
							person.setSkills(Lists.newArrayList(newITSkillSet));
						}else
						{
							emptyITSkillSet.add(util.replaceUnicodeWithASCII(skills.replace(" ", "").trim().toLowerCase()));
							person.setSkills(Lists.newArrayList(emptyITSkillSet));
						}
					}

				}
				if(!util.isEmpty(designationSection) && !util.isEmpty(designation))
				{
					person.setDesignation(util.replaceUnicodeWithASCII(designation[0].replace(" ", "").trim()));
				}
				person.setEmails(emails);
			}
		}

		if(!util.isEmpty(highestEdu))
		{
			List<Education> allEducation = new ArrayList<Education>();
			Education highestEducation = getEducationDetails(highestEdu);
			//person.setEducation(highestEducation);
			if(!util.isEmpty(highestEducation.getDegree()) || !util.isEmpty(highestEducation.getMajor()) || !util.isEmpty(highestEducation.getName()))
			{
				allEducation.add(highestEducation);
			}
			if(!util.isEmpty(secondHighestEdu) && secondHighestEdu.contains("Highest:"))
			{
				Education secondHighestEducation = getEducationDetails(secondHighestEdu);
				if(!util.isEmpty(secondHighestEducation.getDegree()) || !util.isEmpty(secondHighestEducation.getMajor()) || !util.isEmpty(secondHighestEducation.getName()))
				{
					allEducation.add(secondHighestEducation);
				}
			}
			person.setEducation(allEducation);
		}



		person.setSource(CrawledSource.MONSTER.name());



		System.out.println("\n\n\n\n\n\n"+"Person::::"+person.toJSON());
		return person;
	}

	private PersonalDetails getPersonBasicInfo(String otherbasicInfoSection) {
		PersonalDetails personalDetails = new PersonalDetails();
		String[] dobSection =null;
		String[] dob =null;
		String[] gender =null;
		String nationality =null;
		String[] language =null;


		try{

			if(!util.isEmpty(otherbasicInfoSection))
			{
				dobSection = otherbasicInfoSection.split("Date of Birth:");
				if(otherbasicInfoSection.contains("Date of Birth:") && otherbasicInfoSection.contains("Gender:"))
				{
					dob = dobSection[1].split("Gender:");
					if(!util.isEmpty(dob))
					{
						DateOfBirth dateOfBirth = new DateOfBirth();
						dateOfBirth.setDisplay(util.replaceUnicodeWithASCII(dob[0].replace(" ", "").replace(",", "").trim()));
						personalDetails.setDateOfBirth(dateOfBirth);
					}
				}
				if(otherbasicInfoSection.contains("Gender:") && otherbasicInfoSection.contains("Language:"))
				{
					gender = dob[1].split("Language:");
					if(!util.isEmpty(gender))
					{
						personalDetails.setGender(util.replaceUnicodeWithASCII(gender[0].replace(" ", "").replace(",", "").trim()));
					}
				}
				if(otherbasicInfoSection.contains("Language:") && otherbasicInfoSection.contains("Gender:"))
				{
					language = gender[1].split("Nationality:");
					if(!util.isEmpty(language[0]))
					{

						if(language[0].contains(","))
						{
							String[] languages = language[0].split(",");

							for(int i=0;i<languages.length;i++)
							{
								Language lang = new Language();
								lang.setName(util.replaceUnicodeWithASCII(languages[i].replace(" ", "").replace(",", "").trim()));
								allLanguages.add(lang);
							}

						}else
						{
							Language lang = new Language();
							lang.setName(util.replaceUnicodeWithASCII(language[0].replace(" ", "").replace(",", "").trim()));
							allLanguages.add(lang);
						}
					}
				}
				if(otherbasicInfoSection.contains("Nationality:") && language != null)
				{
					nationality = language[1];
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

		return personalDetails;
	}

	private static Education getEducationDetails(String education)
	{
		Education educatn = new Education();
		try{
			if(!util.isEmpty(education) )
			{
				String[] degreeSection =null;
				String[] degree =null;
				String[] majorSection =null;
				String[] endDate =null;
				String[] endDateSection = null;
				String[] insti = null;

				if(education.contains("Highest:") && education.contains("Passed in:"))
				{
					degreeSection = education.split("Passed in");
					degree = degreeSection[0].trim().split("Highest:");
					//degree is in degree[1]
				}

				if(education.contains("Highest:") && !(education.contains("Passed in:")) &&education.contains("Institute:"))
				{
					degreeSection = education.split("Institute:");
					degree = degreeSection[0].split("Highest:");
					//degree is in degree[1]
				}
				if(education.contains("Highest:") && !(education.contains("Passed in:")) &&!(education.contains("Institute:")))

				{
					degreeSection = education.trim().split("Highest:");
					degree = degreeSection[1].trim().split("Highest:");

					//degree is in degree[1]

				}
				
				if(!education.contains("Highest:") && !(education.contains("Passed in:")) &&!(education.contains("Institute:")))

				{
					degree= null;
				}
				if(degree.length > 1)
				{
					if(degree[1].toString().contains("-"))
					{
						majorSection = degree[1].split("-");
						educatn.setDegree(util.replaceUnicodeWithASCII(majorSection[0].replace(" ", "").replace(",", "").trim()));
						educatn.setMajor(util.replaceUnicodeWithASCII(majorSection[1].replace(" ", "").replace(",", "").trim()));
						//major is in majorSec[1]
						//degree is in majorSec[0]
					}else{
						educatn.setDegree(util.replaceUnicodeWithASCII(degree[1].replace(" ", "").replace(",", "").trim()));
					}
				}else if(degree[0]!=null)
				{
					educatn.setDegree(util.replaceUnicodeWithASCII(degree[0].replace(" ", "").replace(",", "").trim()));
				}

				if(education.contains("Passed in:") && education.contains("Institute:"))
				{
					endDateSection = education.split("Institute:");
					endDate = endDateSection[0].split("Passed in:");
					educatn.setEnd(util.replaceUnicodeWithASCII(endDate[1].replace(" ", "").replace(",", "").trim()));
				}

				if(education.contains("Passed in:") && !(education.contains("Institute:")))
				{
					endDate = education.split("Passed in:");
					educatn.setEnd(util.replaceUnicodeWithASCII(endDate[1].replace(" ", "").replace(",", "").trim()));
				}

				if(education.contains("Institute:"))
				{
					insti = education.split("Institute:");
					educatn.setName(util.replaceUnicodeWithASCII(insti[1].replace(" ", "").replace(",", "").trim()));
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return educatn;
	}


	private static List<Experience> getPersonExperience(Map<ExperienceSection, Object> workHistoryMap) {
		List<Experience> experienceList=new ArrayList<>();
		try{
			if (null != workHistoryMap) {
				int experienceCount=0;
				Set<Map.Entry<ExperienceSection, Object>> entries = workHistoryMap.entrySet();

				for (Map.Entry<ExperienceSection, Object> entry : entries) {

					System.out.println(entry.getKey());
					System.out.println(entry.getValue());
				}
				Set<Map<ExperienceDetailsSection,Object>> experienceDetailsSectionObjectMap= (Set<Map<ExperienceDetailsSection, Object>>) workHistoryMap.get(ExperienceSection.EXPERIENCE_DETAILS_SECTION);

				for (Map<ExperienceDetailsSection, Object> item : experienceDetailsSectionObjectMap) {
					Experience experience=new Experience();
					experience.setResumeOrdinal(experienceCount);
					experienceCount=experienceCount+1;
					String company = (String) item.get(ExperienceDetailsSection.COMPANY);
					String designation = (String) item.get(ExperienceDetailsSection.DESIGNATION);
					String duration=(String) item.get(ExperienceDetailsSection.DURATION);


					if (!util.isEmpty(company))
						company = company.replace("(Current)", "");
					experience.setOrg(util.replaceUnicodeWithASCII(company.replace(" ", "").replace(",", "").trim()));
					if (!util.isEmpty(designation))
						experience.setTitle(util.replaceUnicodeWithASCII(designation.replace(" ", "").replace(",", "").trim()));
					if (!util.isEmpty(duration)) 
					{
						System.out.println("+++++++++"+duration);
						String timePeriod=util.replaceUnicodeWithASCII(duration);
						System.out.println("time period is"+timePeriod);
						if (timePeriod.contains(" to ")) {
							System.out.println("inside if");
							String[] durationSplit = timePeriod.split(" to ");
							experience.setStart(util.replaceUnicodeWithASCII(durationSplit[0].replace(" ", "").replace(",", "").trim()));
							if(durationSplit[1].contains("Till date")){
								experience.setEnd("Present");
								experience.setCurrent(true);
							}else{
								experience.setEnd(util.replaceUnicodeWithASCII(durationSplit[1].replace(" ", "").replace(",", "").trim()));
							}
						}
						else{
							experience.setEnd(util.replaceUnicodeWithASCII(timePeriod.replace(" ", "").replace(",", "").trim()));
						}
					}

					experienceList.add(experience);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return experienceList;
	}

	private static Name getPersonName(String name) {
		Name personName=new Name();
		try{
			if(!util.isEmpty(name)) {
				String[] names = name.split(" ", 2);
				personName.setGiven_name(util.replaceUnicodeWithASCII(names[0].replace(",", "").trim()));
				if(names.length>1)
					personName.setFamily_name(util.replaceUnicodeWithASCII(names[1].replace(",", "").trim()));
				return personName;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/*public static void main(String[] args) throws Exception
	{
		String fileContent=readFile("/Users/anuroop/Downloads/Monster profiles/Jeyasekar Rajamani - Bengaluru _ Bangalore, 22 years.html");
		MonsterParser parser=new MonsterParser();
		if(null!=fileContent)
			parser.parsePerson(fileContent);
		else
			System.out.println("user is deactivated");
	}

	public static String readFile(String localFile) throws Exception 
	{
		FileReader fileReader = new FileReader(localFile);
		BufferedReader reader = new BufferedReader(fileReader);
		StringWriter stringWriter = new StringWriter();
		String line;
		while ((line = reader.readLine()) != null)
			stringWriter.write(line);
		return stringWriter.toString();
	} */

}

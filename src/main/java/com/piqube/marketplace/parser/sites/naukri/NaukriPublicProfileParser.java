package com.piqube.marketplace.parser.sites.naukri;

import com.google.common.collect.Lists;
import com.piqube.marketplace.parser.CrawledSource;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils.ParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;
import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.marketplace.parser.utils.UnaryFunction;
import com.piqube.model.*;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by jenefar on 08/02/16.
 */
public class NaukriPublicProfileParser implements Parser {
    public static Logger log = LoggerFactory.getLogger(NaukriPublicProfileParser.class);

    static FileUtil util =new FileUtil();
    public static final String monthsRegex = "(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)";
    public static final String dateRegex =
            String.format( "(?i)(?<startMonth>%s)[\\s]+(?<startYear>[\\d]{4})[\\s]+to[\\s]+((?<endMonth>%s)[\\s]+(?<endYear>[\\d]{4})|(?<current>till date))", monthsRegex, monthsRegex );

    public  enum Sections implements IEnumValue<_SelectorMetaInfo> {

        NAME_SELECTOR(  new _SelectorMetaInfo(Arg.E(".cvPrev h1") ) ),
        DESIGNATION_SELECTOR( new _SelectorMetaInfo(Arg.E(".tupBGN li:contains(Current Designation)"),
                Arg.F(new ParseUtils.NameValueSplit()))),
        LOCATION_SELECTOR(  new _SelectorMetaInfo( Arg.E(".tupBGN li:contains(Current Location)"),
                Arg.F(new ParseUtils.NameValueSplit()) ) ),
        PHOTO_SELECTOR( new _SelectorMetaInfo( Arg.E(".jseekImg[src]"), Arg.a("src") ) ),
        EMAIL_SELECTOR( new _SelectorMetaInfo( Arg.E(".tupBGN li:contains(Email) span"), Arg.ownText(), Arg.list() ) ) ,

        SUMMARY_SELECTOR( new _SelectorMetaInfo( Arg.E("#container .section") )),
        SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E("#profile-skills ul li .endorse-item-name a "),Arg.list())),
           CURRENT_COMPANY( new _SelectorMetaInfo( Arg.E("#headline .title") ) ),

        PROFILE_URL(new _SelectorMetaInfo( Arg.E("link[rel=canonical]"),Arg.a("href"))),
        KEY_SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E(".tupBGN li:contains(Key Skills)"),
                Arg.F(new ParseUtils.NameValueSplit()) ) ),
        IT_SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E(".section:contains(IT Skills) .tableDMNR tr td:eq(0)"), Arg.list() ) ),


        EXPERIENCE_SELECTOR( new _SelectorMetaInfo(
                Arg.E("h2:contains(Work Experience) ~ * li"), Arg.list(),
                Arg.Children(ExperienceSection.class) ) ),

        EDUCATION_SELECTOR( new _SelectorMetaInfo( Arg.E("h2:contains(Education) ~ * li"),Arg.list(),
                Arg.Children(EducationSection.class) ) ),
;
      /**  EXPERIENCE_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-experience [id^=experience-]"),Arg.list(),
                Arg.Children(ExperienceSection.class) ) ),

        PROJECT_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-projects [id^=project-]"),Arg.list(),
                Arg.Children(ProjectSection.class) ) ),

        AWARD_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-honors .honoraward"),Arg.list(),
                Arg.Children(AwardSection.class) ) ),
        CERTIFICATION_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-certifications [id^=certification-]"),Arg.list(),
                Arg.Children(CertificationSection.class) ) ),
        COURSE_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-courses .section-item"),Arg.list(),
                Arg.Children(CourseSection.class) ) ),

        ORGANIZATION_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-organizations [id^=organization-]"),Arg.list(),
                Arg.Children(OrganizationSection.class) ) ),
        TEST_SCORES_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-test-scores [id^=scores-]"),Arg.list(),
                Arg.Children(TestScoresSection.class) ) ),
        VOLUNTEER_EXPERIENCE_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-volunteering [id^=volunteering-] .experience"),Arg.list(),
                Arg.Children(VolunteerSection.class) ) ),
        VOLUNTEER_OPPORTUNITIES_SELECTOR( new _SelectorMetaInfo( Arg.E("#volunteering-opportunities .opportunities li"),Arg.list() )),

        VOLUNTEER_CAUSES_SELECTOR( new _SelectorMetaInfo( Arg.E("#volunteering-interests .interests li"),Arg.list() )),

        VOLUNTEER_ORGANIZATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".non-profits .volunteering-listing li"),Arg.list() )),

        PATENT_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-patents [id^=patent-]"),Arg.list(),
                Arg.Children(PatentSection.class) ) ),
        LANGUAGE_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-languages [id^=languages-] ol li"),Arg.list(),
                Arg.Children(LanguageSection.class) ) ),
        PUBLICATION_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-publications [id^=publication-]"),Arg.list(),
                Arg.Children(PublicationSection.class) ) ),
*/


        ;
        private _SelectorMetaInfo metaInfo;

        Sections(_SelectorMetaInfo info) {
            this.metaInfo = info;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }

    }


    public enum ExperienceSection implements IEnumValue<_SelectorMetaInfo> {

        COMPANY( new _SelectorMetaInfo<String,Element>(
                Arg.E(":matchesOwn(as)"), Arg.F(new ParseUtils.RegexFetcher("company", "(?<company>(.*?))as(.*)"))) ),
        DURATION( new _SelectorMetaInfo( Arg.E(String.format(":matchesOwn(%s)",dateRegex) ) ) ),

        DESIGNATION(new _SelectorMetaInfo( Arg.E("em[class=f14]") ) ),

        DESCRIPTION(new _SelectorMetaInfo( Arg.E("span[class^=cls]") ) ),


        ;
        public _SelectorMetaInfo metaInfo;
        private ExperienceSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {


        DEGREE(new _SelectorMetaInfo( Arg.E("strong") ) ),
        MAJOR(new _SelectorMetaInfo( Arg.E("strong ~strong") ) ),

        UNIVERSITY(new _SelectorMetaInfo( Arg.E("strong ~strong ~strong")  ) ),
        DURATION(new _SelectorMetaInfo( Arg.E("strong ~strong ~strong ~strong")  ) ),

        SECTION(new _SelectorMetaInfo( Arg.E("li") ) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private EducationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public  enum ProjectSection implements IEnumValue<_SelectorMetaInfo> {

        NAME(new _SelectorMetaInfo( Arg.E("h4 span")) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".projects-date")) ),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".description"), Arg.text() ) ),
        PROJECT_LINK(new _SelectorMetaInfo( Arg.E("h4 a"),Arg.a("href") ) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private ProjectSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }



    public static class MemberIdNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            if (input.contains("-")) {
                String[] str = input.split("-");
                if (str.length > 0)
                    return str[1].trim();
                return null;
            } else {
                return null;
            }
        }
    }
    @Override
    public Person parsePerson(String htmlSource) throws ParsingException {

        Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
        Set<Map.Entry<Sections, Object>> entries = map.entrySet();
        for (Map.Entry<Sections, Object> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        Set<Map<EducationSection, Object>> educationFieldMap = (Set<Map<EducationSection, Object>>) map.get(Sections.EDUCATION_SELECTOR);
        Set<Map<ExperienceSection, Object>> experienceFieldMap = (Set<Map<ExperienceSection, Object>>) map.get(Sections.EXPERIENCE_SELECTOR);

        String skills = (String) map.get(Sections.KEY_SKILLS_SELECTOR);
        List<String> skillSet = null;
        if(null!=skills) {
            skillSet = Arrays.asList(skills.split("\\s*,\\s*"));
        }
        Set<String> ITSkillSet=(Set)map.get(Sections.IT_SKILLS_SELECTOR);
        String name = (String) map.get(Sections.NAME_SELECTOR);
        String location = (String) map.get(Sections.LOCATION_SELECTOR);
        String profileSummary = (String) map.get(Sections.SUMMARY_SELECTOR);
        String currentCompany = (String) map.get(Sections.CURRENT_COMPANY);
        String photoUrl = (String) map.get(Sections.PHOTO_SELECTOR);
        String profileUrl = (String) map.get(Sections.PROFILE_URL);
        String currentDesignation = (String) map.get(Sections.DESIGNATION_SELECTOR);


        Person person = new Person();

        if (!util.isEmpty(name)) {
            Name personName = getPersonName(name);
            person.setName(personName);
        }
        if (!util.isEmpty(profileSummary))
            person.setProfileSummary(util.replaceUnicodeWithASCII(profileSummary));

        if(!util.isEmpty(profileUrl))
            person.setProfileUrl(profileUrl);


        if(!util.isEmpty(photoUrl))
            person.setPhoto_url(photoUrl);
        if(!util.isEmpty(currentDesignation))
            person.setDesignation(currentDesignation);
        if(!util.isEmpty(currentCompany))
            person.setCurrentCompany(currentCompany);
        if(!util.isEmpty(location))
            person.setLocality(location);


        if(!util.isEmpty(skillSet)) {
            Set<String> skill = new HashSet<>(skillSet);
            person.setSkills(Lists.newArrayList(skill));
            /*if (ITSkillSet.size() > 0) {
                ITSkillSet.addAll(skillSet);
                Set<String> skill = new HashSet<>(ITSkillSet);
                person.setSkills(Lists.newArrayList(skill));
            } else {
                Set<String> skill = new HashSet<>(skillSet);
                person.setSkills(Lists.newArrayList(skill));
            }*/
        }

        if (!util.isEmpty(educationFieldMap)) {
            List<Education> educationList = getPersonEducation(educationFieldMap);
            person.setEducation(educationList);
        }
        if (!util.isEmpty(experienceFieldMap)) {
            List<Experience> experienceList = getPersonExperience(experienceFieldMap);
            person.setExperience(experienceList);
        }
        //person.computeAndSetMobility();
       // person.computeAndSetStability();
        person.setSource(CrawledSource.LINKEDIN.name());
        person.setCrawledDate(new Date());

        System.out.println("Person:"+person.toJSON());
        return person;
    }



    private static List<Experience> getPersonExperience(Set<Map<ExperienceSection, Object>> experienceFieldMap) {
        List<Experience> experienceList=new ArrayList<>();
        if (null != experienceFieldMap) {
            int experienceCount=0;
            for (Map<ExperienceSection, Object> item : experienceFieldMap) {
                Experience experience=new Experience();
                experience.setResumeOrdinal(experienceCount);
                experienceCount=experienceCount+1;
                String company = (String) item.get(ExperienceSection.COMPANY);
                String designation = (String) item.get(ExperienceSection.DESIGNATION);
                String duration=(String) item.get(ExperienceSection.DURATION);
                String description=(String) item.get(ExperienceSection.DESCRIPTION);


                if (!util.isEmpty(company))
                    experience.setOrg(company);
                if (!util.isEmpty(designation))
                    experience.setTitle(designation);
                if (!util.isEmpty(description))
                    experience.setDesc(description);
                if (!util.isEmpty(duration)) {
                    if (duration.contains(" to ")) {
                        Map map = splitString(duration, " to ");
                        experience.setStart((String) map.get("left"));
                        if (map.get("right").toString().contains("Till Date")) {
                            experience.setEnd("Present");
                            experience.setCurrent(true);
                        } else {
                            experience.setEnd(map.get("right").toString());
                        }
                    }
                    else{
                        experience.setEnd(duration);
                    }
                }

                experienceList.add(experience);
            }
        }
        return experienceList;
    }

    private static List<Education> getPersonEducation(Set<Map<EducationSection, Object>> educationFieldMap) {
        List<Education> educationList = new ArrayList<>();
        if (null != educationFieldMap) {
            for (Map<EducationSection, Object> item : educationFieldMap) {
                Education education = new Education();
                String university = (String) item.get(EducationSection.UNIVERSITY);
                String degree = (String) item.get(EducationSection.DEGREE);
                String duration = (String) item.get(EducationSection.DURATION);
                String major = (String) item.get(EducationSection.MAJOR);

                if (!util.isEmpty(university)) {
                    if(!university.contains("Other Qualifications")) {
                        education.setName(university.replace(",", ""));
                        if (!util.isEmpty(degree))
                            education.setDegree(degree.replace(",", ""));
                        if (!util.isEmpty(major))
                            education.setMajor(major.replace("(", "").replace(")", "").replace(",", ""));
                        if (!util.isEmpty(duration))
                            education.setEnd(duration);

                        educationList.add(education);
                    }
                }
            }
        }
        return educationList;
    }

    private static Map splitString(String input,String delimiter) {
        Map map = new HashMap();
        if (null != input) {
            String[] words = input.split(delimiter);
            map.put("left", words[0]);
            if (words.length > 1)
                map.put("right", words[1]);
            return map;
        }
        return null;
    }

    private static Name getPersonName(String name) {
        Name personName=new Name();

        if(!util.isEmpty(name)) {
            String[] names = name.split(" ", 2);
            personName.setGiven_name(names[0]);
            if(names.length>1)
                personName.setFamily_name(names[1]);
            return personName;
        }
        return null;
    }

  /*  public static void main(String[] args) throws Exception {

String fileContent=readFile("/Users/jenefar/workspace/marketplacedev/extensionservice/src/main/java/com/piqube/marketplace/parser/sites/naukri/test1.html");
        NaukriPublicProfileParser parser=new NaukriPublicProfileParser();
        if(null!=fileContent)
            parser.parsePerson(fileContent);
        else
            System.out.println("user is deactivated");

    }
    public static String readFile(String localFile) throws Exception {
        FileReader fileReader = new FileReader( localFile );
        BufferedReader reader = new BufferedReader(fileReader);
        StringWriter stringWriter = new StringWriter();
        String line;
        while( ( line = reader.readLine() ) != null )
            stringWriter.write(line);
        return stringWriter.toString();
    }*/


}



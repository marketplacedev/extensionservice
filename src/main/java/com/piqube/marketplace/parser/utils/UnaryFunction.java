package com.piqube.marketplace.parser.utils;

public interface UnaryFunction<Result,Arg> {
    public Result exec(Arg input);
}

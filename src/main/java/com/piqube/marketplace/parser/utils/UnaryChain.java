package com.piqube.marketplace.parser.utils;

public class UnaryChain<Result,Intermediate,Arg> implements UnaryFunction<Result,Arg> {

    public UnaryFunction<Intermediate,Arg>     left;
    public UnaryFunction<Result,Intermediate> right;

    public UnaryChain(UnaryFunction<Intermediate,Arg> left) {
        this.left = left;
    }

    public UnaryFunction<Result,Arg> chain(UnaryFunction<Result,Intermediate> right) {
        this.right = right;
        return this;
    }

    @Override
    public Result exec(Arg arg) {
        Intermediate intermediate = left.exec(arg);
        if ( intermediate == null )
            return null;
        return right.exec(intermediate);
    }
}

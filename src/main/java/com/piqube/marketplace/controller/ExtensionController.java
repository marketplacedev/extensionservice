package com.piqube.marketplace.controller;

import com.google.gson.Gson;
import com.piqube.marketplace.model.ExtensionProfile;
import com.piqube.marketplace.model.Users;
import com.piqube.marketplace.parser.sites.linkedin.LinkedinParser;
import com.piqube.marketplace.parser.sites.monster.MonsterParser;
import com.piqube.marketplace.parser.sites.naukri.NaukriProfileParser;
import com.piqube.marketplace.parser.sites.naukri.NaukriPublicProfileParser;
import com.piqube.marketplace.parser.sites.shine.ShineParser;
import com.piqube.marketplace.parser.sites.timesjobs.TimesJobsProfileParser;
import com.piqube.marketplace.service.ExtensionService;
import com.piqube.marketplace.service.UserService;
import com.piqube.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import org.springframework.web.client.RestTemplate;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by jenefar on 20/01/16.
 */
@RestController
@EnableRedisHttpSession
@EntityScan(basePackages="com.piqube")
@EnableJpaRepositories({"com.piqube.marketplace.repository"})

//@RequestMapping("/extension")
public class ExtensionController {


    @Autowired
    ExtensionService extensionService;

    @Autowired
    UserService userservice;

    @Value("${profile.html.location}")
    String profileHtmlLocation;

    private static final Logger LOGGER = LoggerFactory.getLogger(ExtensionController.class);

    @RequestMapping(value="/extension/check", method = RequestMethod.GET)
    @ResponseBody
    public String check(@RequestParam(value="url", required=true)String pageUrl) throws MalformedURLException {
        LOGGER.debug("====inside check:" + pageUrl);

        return "SUCCESS";
    }
        @RequestMapping(value="/extension/can-open-extension", method = RequestMethod.GET)
    @ResponseBody
    public String canOpenExtension(@RequestParam(value="url", required=true)String pageUrl) throws MalformedURLException {
        LOGGER.debug("====inside canOpenExtension:"+pageUrl);

        Boolean retVal = Boolean.FALSE;
        if(pageUrl == null) {
            return retVal.toString();
        }
        try {
            LOGGER.debug("====pageurl:"+pageUrl);

            //temp fix
            pageUrl=pageUrl.replaceAll("-","_");

            LOGGER.debug("page url is:::::::::"+pageUrl);
            URL url = new URL(pageUrl);
            String host  = url.getHost();
            String path = url.getPath();
            LOGGER.debug("host url is:::::"+host);
            LOGGER.debug("path url is:::::"+path);
            if(host.matches(".*.linkedin.com") || host.matches(".*linkedin.com")) {
                if(path.matches("/pub/dir/.*")) {
                    retVal = Boolean.FALSE;
                } else if(path.matches("/\\w{2}/\\w{1,50}") || path.matches("/pub/.*") || path.matches("/profile/view.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.naukri.com") || host.matches(".*naukri.com")) {
                if(path.matches(".*/preview/preview.*") || path.matches("/preview/preview.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.monsterindia.com") || host.matches(".*monsterindia.com")) {
                if(path.matches("/v2/resumedatabase/resume.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.timesjobs.com") || host.matches(".*timesjobs.com")) {
                if(path.matches("/employer/resumeDetailView.html.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.shine.com") || host.matches(".*shine.com")) {
                LOGGER.debug("Shine Path url:{}",path);
                if(path.matches("/search/profile/.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            /*else if (host.matches(".*.github.com") || host.matches(".*github.com")) {
                if(path.matches("/.*//*.*")) {
                    retVal = Boolean.FALSE;
                }else if(path.matches("/.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.naukri.com") || host.matches(".*naukri.com")) {
                if(path.matches("/preview/preview.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.monsterindia.com") || host.matches(".*monsterindia.com")) {
                if(path.matches("/v2/resumedatabase/.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.stackoverflow.com") || host.matches(".*stackoverflow.com")) {
                if(path.matches("/users/.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.twitter.com") || host.matches(".*twitter.com")) {
                if(path.matches("/.*//*.*")) {
                    retVal = Boolean.FALSE;
                }else if(path.matches("/.*")) {
                    retVal = Boolean.TRUE;
                }
            }

            else if (host.matches(".*.employer.dice.com") || host.matches(".*employer.dice.com")) {
                if(path.matches("/.*")) {
                    LOGGER.debug("true");
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.recruiter.wisdomjobs.com") || host.matches(".*recruiter.wisdomjobs.com")) {
                if(path.matches("/.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.jobstreet.co") || host.matches(".*jobstreet.co.*")) {
                LOGGER.debug("inside jobstreet:::::::::::::::::::");
                if(path.matches("/.*employer.*")) {
                    LOGGER.debug("inside jobstreet if");
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.jobsdb.com") || host.matches(".*jobsdb.com")) {
                if(path.matches(".*Employer.*") || path.matches(".*employer.*")) {
                    LOGGER.debug("inside jobsdb.com if");
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.monster.com") || host.matches(".*monster.com")) {
                if(path.matches("/v2/resumedatabase/.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.recruit.com") || host.matches(".*recruit.com")) {
                if(path.matches("/.*employer.*")) {
                    retVal = Boolean.TRUE;
                }
            }*/
        } catch (MalformedURLException e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            retVal = Boolean.FALSE;
        }

            LOGGER.debug("return value is::::"+retVal.toString());
        return retVal.toString();
    }

    @RequestMapping(value="/extension/get-request-id", method=RequestMethod.GET)
    @ResponseBody
    public String getUUID() {
        String uuid = java.util.UUID.randomUUID().toString();
        return uuid;
    }

    @RequestMapping(value="/extension/verify-user", method=RequestMethod.POST)
    @ResponseBody
    public String verifyUser(@RequestParam String username) {
        Users user=userservice.getUserByEmail(username);
        if(null!=user)
            return Boolean.TRUE.toString();
        else
            return Boolean.FALSE.toString();
    }
    @RequestMapping(value="/extension/{requestId}/upload", method=RequestMethod.POST)
    @ResponseBody
    public String fileUpload(byte[] file, @RequestParam(required = true, value = "url") String url, @PathVariable String requestId, HttpServletRequest request )
            throws Exception {
        LOGGER.debug("File upload 'Post' method...");

        // validate a file was entered
        if (file.length == 0) {
            LOGGER.debug("File content length empty...");
            return Boolean.FALSE.toString();
        }

        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        CommonsMultipartFile multipartFile = (CommonsMultipartFile) multipartRequest.getFile("file");

        // the directory to upload to
        String uploadDir = profileHtmlLocation;

        // Create the directory if it doesn't exist
        File dirPath = new File(uploadDir);

        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }

        //retrieve the file data
        InputStream stream = null;
        //write the file to the file specified
        OutputStream bos = null;
        String outFilePath = null;
        if(multipartFile != null && !multipartFile.isEmpty()) {
            outFilePath = uploadDir + multipartFile.getOriginalFilename();
            stream = multipartFile.getInputStream();
            bos = new FileOutputStream(outFilePath);
        } else {
            outFilePath = uploadDir + requestId;
            stream = new ByteArrayInputStream(file);
            bos = new FileOutputStream(outFilePath);
        }
        int bytesRead;
        byte[] buffer = new byte[8192];

        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
            bos.write(buffer, 0, bytesRead);
        }

        bos.close();

        //close the stream
        stream.close();

        LOGGER.debug("url is::::"+url);
        LOGGER.debug("====filepath is====="+outFilePath);
        String fileContent=readFile(outFilePath);
        Person person = null;
        if(url.contains("linkedin")) {
            LinkedinParser parser = new LinkedinParser();
            person=parser.parsePerson(fileContent);

        }else if(url.contains("freesearch")){
            NaukriPublicProfileParser parser=new NaukriPublicProfileParser();
            person=parser.parsePerson(fileContent);

        }
        else if(url.contains("timesjobs")){
            TimesJobsProfileParser parser=new TimesJobsProfileParser();
            person=parser.parsePerson(fileContent);

        }
        else if(url.contains("monster")){
            MonsterParser parser=new MonsterParser();
            person=parser.parsePerson(fileContent);

        }
        else if(url.contains("resdex"))
        {
            NaukriProfileParser parser=new NaukriProfileParser();
            person=parser.parsePerson(fileContent);
        }
        else if(url.contains("shine.com")){
            ShineParser parser=new ShineParser();
            person=parser.parsePerson(fileContent);

        }

        if(null!=person) {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType( MediaType.APPLICATION_JSON );

            LOGGER.debug("======JSON gson=====\n"+new Gson().toJson(person));

            HttpEntity httpRequest= new HttpEntity( person.toJSON(), headers );
            LOGGER.debug("======JSON=====\n"+person.toJSON());

            RestTemplate template = new RestTemplate();

          //  Person createdProfile=template.postForObject( "http://127.0.0.1:8002/profile/test", httpRequest, Person.class );
          // template.postForObject("http://127.0.0.8002/profiletest",person,Person.class);
            person.set_id(UUID.randomUUID().toString());
            createProfile(person);
            LOGGER.debug("successfully posted::::");

            ExtensionProfile profile=new ExtensionProfile();
            profile.setRequestId(requestId);
            profile.setFilePath(outFilePath);
            profile.setUrl(person.getProfileUrl());
            profile.setProfileId(person.get_id());

            extensionService.saveExtensionProfile(profile);
        }

        return Boolean.TRUE.toString();
    }



    public void createProfile(Person person) {

        extensionService.saveProfile(person);
        LOGGER.debug("**************"+person.toJSON());
    }
    public static String readFile(String localFile) throws Exception {
        //FileReader fileReader = new InputStreamReader(new FileInputStream(localFile), "UTF-8");
        //BufferedReader reader = new BufferedReader(fileReader);
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(localFile), "UTF-8"));
        StringWriter stringWriter = new StringWriter();
        String line;
        while( ( line = br.readLine() ) != null )
            stringWriter.write(line);
        return stringWriter.toString();
    }

    @RequestMapping(value="/extension/load",method=RequestMethod.GET)
    public ModelAndView loadProfile(@RequestParam(required = true, value = "requestid") String requestId,
                                    @RequestParam(required = true, value = "url") String url, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {

        LOGGER.debug("forwarding to extension html......");
        LOGGER.debug("requestId:"+requestId);
        LOGGER.debug("url is:"+url);
        ModelAndView modelAndView=new ModelAndView();
        Map<String, Serializable> model = new HashMap<String, Serializable>();
        model.put("ctx", request.getContextPath());
        LOGGER.debug("=======Redirecting========");
        modelAndView.setViewName("index");
        modelAndView.addObject("message","extension");
        return modelAndView;
    }
    @RequestMapping(value="/extension/profile",method=RequestMethod.POST)
    public String postProfile(@RequestParam String requestId, @RequestParam String username) throws Exception {

        LOGGER.debug("extension post is called with request:::::" + requestId);
        LOGGER.debug("LOGGED in user is:"+username);
        List<ExtensionProfile> extensionProfiles = extensionService.getProfileByRequestId(requestId.replace("&", ""));

        if (null != extensionProfiles) {
            LOGGER.debug("profile id is::::" + extensionProfiles.get(0).getProfileId());

            Person person = extensionService.getExtensionProfile(extensionProfiles.get(0).getProfileId());
            Users user=userservice.getUserByEmail(username);
            if(null!=user)
                extensionProfiles.get(0).setCreatedBy(user);
            LOGGER.debug("person id is::::" + person.get_id());
            extensionProfiles.get(0).setReferred(true);
            extensionService.saveExtensionProfile(extensionProfiles.get(0));

            return new Gson().toJson(person);
        } else {
            return "FAILURE";
        }

    }
}
